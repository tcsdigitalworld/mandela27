<?php

include('_header.php');

?>
	<div class="maincontainer">
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Events</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">10 oct</span><span class="year">2014</span></div>
						<h2>Afrovibes and The Drum are proud to host Mandela 27</h2>
						<p class="smalltitleorange">Birmingham, UK</p>
					</div>
					<div class="clear"></div><br />
					<p class="short_intro">The Mandela27 exhibition opened at the Drum Arts centre in Birmingham as part of their Afro Vibes festival.</p>
					<p>The installation is being displayed alongside some amazing photographs by Dr Vanley Burke from his time in SA during the 1990s. Dr Burke travelled extensively throughout South Africa to Soweto, Sharpeville, Bophuthatswana, Cape Town and Durban, talking to individuals and families, attending rallies, funerals and demonstrations, and capturing a unique insight of a nation undergoing immense change towards eventual democracy.</p>
					<p><img src="files/events-afrovibes.jpg" /></p>
					<div class="backtoteam bottom">
						<a href="events.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
