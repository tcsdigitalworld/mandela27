<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Past Events</div>
					<img src="images/years-right.png" />
				</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="title">
						<h2>PAST MANDELA27 EVENTS </h2>
					</div>
					<p class="short_intro">The launch of the Mandela27 project and more specifically the Exhibitions have been big news in Europe and South Africa. Check our <a href="events.php">News-section</a> for articles about our events. </p>
					<p><img src="images/events-map-past.jpg" alt="Past Events Map" /></p><br />
					<div class="clear"></div>

					<p><strong>DIY EXHIBITIONS:</strong><img src="images/redthumbtack.png" /></p>
					<p><strong>BELGIUM:</strong><br />
						<a href="http://belvue.be/en" target="_blank">- BELVUE Museum, Brussels (7 January - 16th March 2014)</a><br />
						<a href="http://visitbrussels.be/bitc/BE_en/monument/787/hotel-de-ville-de-bruxelles.do" target="_blank">- City Hall, Brussels (21 March 2014)</a><br />
						- Aula Ghent University, Ghent (15 October 2014)
					</p>
					<p><strong>UNITED KINGDOM:</strong><br />
						<a href="http://www.theherbert.org/" target="_blank">- The Herbert Art Gallery &amp; Museum, Coventry  (11 April - 1 June 2014)</a><br />
						- Cape Cornwall School, Coventry (7 – 25 July 2014)<br />
						- The Drum, Birmingham (13 – 25 October 2014)
					</p>
					<p><strong>DENMARK:</strong><br />
						<a href="http://www.gaiamuseum.dk" target="_blank">- GAIA Museum, Randers (3 July – 31 October 2014)</a>
					</p>
					<p><strong>SWEDEN:</strong><br />
						- Vallentuna Kulturhus, Stockholm (3 September – 20 October 2014<br />
						- Almedals Bibliotek, Visby (3 September – 20 Oktober 2014)
					</p>
					<p><strong>ITALY:</strong><br />
						- Istituto d'Istruzione Superiore Silvio D'Arzo, Montecchio Emilia (20 October – 20 December 2014)
					</p>
					<p><strong>SOUTH AFRICA:</strong><br />
						<a href="http://www.robben-island.org.za/" target="_blank">- Robben Island Museum, Cape Town (18 July 2014, Mandela Day)</a><br />
						- National Library of South Africa, Pretoria (18 July 2014, Mandela Day - 31 July 2014)<br />
						- Drakenstein Correctional Services (18 July 2014, Mandela Day)<br />
						- Delft Civic Centre, Delft (Starting 18 July 2014, Mandela Day – 25 July 2014)<br />
						- North-West University, Vaal Triangle (1 August – 23 October 2014)<br />
						- Manenberg Community Library, Cape Town (12 – 30 August 2014)<br />
						- Nelson Mandela Gateway to Robben Island, Cape Town (16 – 18 September 2014)<br />
						- Cape Town Central Library, Cape Town (22 September – 3 October 2014)<br />
						- Robben Island Museum, Jetty 1, Cape Town (3 October 2014 – 29 February 2015)<br />
						- False Bay FET College, Khayeltisha (14 – 15 October 2014)<br />
						- False Bay FET College, Mitchell’s Plain (16 – 17 October 2014)<br />
						- Freedom Park, Pretoria (21 October 2014)
					</p>
					<p><strong>TEAM MEETINGS:</strong><img src="images/yellowthumbtack.png" /><br />
					<a href="event-27-11-2012.php">Project Kick-off, Coventry, Great Britain (27 November 2012)</a><br/>
					<a href="event-27-02-2013.php">Project Meeting &amp; Press Launch, Robben Island &amp; Cape Town, South Africa (27 February 2013)</a><br/>
					<a href="event-11-09-2013.php">Project Meeting, Stockholm, Sweden (11 September 2013)</a><br/>
					<a href="event-02-01-2014.php">Official Project Presentation, Brussels, Belgium (31 October 2014)</a></p>
					<div class="backtoteam bottom">
						<a href="project_events.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>			
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
				?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
