<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Learning resources</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="title">
						<h2>Learning resources</h2>
					</div>
				</div>
				
				<img src="files/pic_1.jpg" alt="image" />
				<div class="clear"></div>

				<div class="content">

					
					<p><strong>Media</strong><br/>A video showing the construction of a diy exhibition</p>
					
					<p><strong>Webquest</strong><br/>To support the Cultural Map, the Serious Game and the DIY Exhibition. we will set various webquest problem based tasks to be solved using the Mandela27-material in combination with on-line research.</p>
					
					<p><strong>Notes for teachers</strong><br/>The teacher notes will follow the exhibition panel headings and contain texts, questions and answers for teachers to use with pupils and students. The teacher notes will address the background to Apartheid and the fight for freedom and democracy in South Africa however the main focus will be on what it means for young people to be a citizen today.
</p>	
					<p>The teacher notes will raise the following topics:</p>
					<ul>
						<li>Background: Exploration, colonialism and slavery!</li>
						<li>What is Apartheid?</li>
						<li>Defiance: Sharpeville & Soweto;</li>
						<li>Fight for democracy: The people shall govern!</li> 
						<li>Robben island: Island prison;</li>
						<li>Mandela and the political prisoners: Culture, education and censorship in the prison;</li>
						<li>Don't entertain Apartheid! Support the cultural boycott (IDAF and international solidarity);</li>
						<li>How culture can influence change in society;</li>
						<li>Culture, reconciliation and citizenship.</li>

					</ul>					
					<br/>
					<p>The teacher notes will be available initially in English, Africaans, Xhosa, Swedish and Dutch.</p>
					
					<p><strong>Forum</strong><br/>An on-site forum for teachers to discuss educational aspects of the material and to get feedback from experts.</p>
					<p><strong>Learning network</strong><br/>Teachers will be invited to join a network group to be linked to the existing learning networks run by Elderberry AB at www.smile-vet.eu and www.sharinglandscapes.eu. A sub group will be formed in each forum on the Mandela27-project.</p>
					
				</div>
				<img src="files/pic_2.jpg" alt="image" />
				<div class="clear"></div>
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
				?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
