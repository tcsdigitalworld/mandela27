<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Blogs</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">19 aug</span><span class="year">2014</span></div>
						<h2>Werner, Project partner, Mandela27</h2>
						<div class="clear"></div>
					</div>
					<p class="short_intro"></p>
					<p>The exhibition was hosted at the Library's Pretoria campus.  On that very same day Robben Island Museum also launched the first of several displays in and around Cape Town.The exhibition at the NLSA was opened on Madiba day (18 July 2014) and remained there for 2 weeks.  The NLSA boasts in excess of 1300 visitors per day and up to 3000 visitors on a weekend.  I was personally involved in the building of the display and it was amazing to experience the interest it sparked right from the outset.  I mean, we were still busy building it when the first flood of questions and comments came our way... "What is this?", "Wow, is this really how big the cell was?", "Can we come and look when you are done?", "We want to play the game", "These posters are lovely"... Oh, and I didn't mention that we actually enclosed the build site and tried to keep the public at bay!  None the less, a successful opening followed and all that were there had a blast experiencing the web portal, playing the game and experiencing the cell.</p>
					<p>After two weeks at NLSA, the display travelled to North-West University: Vaal's new library building;  once again a build that generated curiosity and awe.  NWU: Vaal has approximately 5000 face-to-face students, many of which have already experienced the display to its fullest.  We have been considering an event around the presence of the display, but this may not even be necessary... the display attracts its own audience.  SGI-SA has been approached with many congratulations on the display. As much as we enjoy a bit of credit, we are quick to applaud our partners (TCS Digital World - Belgium, Elderberry - Sweden, Robben Island Museum - SA and Coventry University - UK) and the Culture Programme of the European Union that have made it all possible.</p>
					<p>So, is this where it ends? Nope, the excitement around the Mandela27 project was brought to an event called TechnoX that is hosted by Sasol here in the Vaal Triangle.  TechnoX is aimed at bringing local school children (ages from 13 to 18) in contact with technology and innovation institutions from all over South Africa.  SGI-SA secured a week-long daily slot at the event primarily to showcase the Mandela27 serious game and web portal.  We saw about 3 to 4 groups of 50+ school children per day at the event. Oh man, the way they drummed around the screens when the Mandela27 project came on!!</p>
					<p>Where to from here... We are in discussion with the KwaZulu Natal Museum to relocate the display to them in October and from there we will likely take it to the NWU: Potchefstroom Campus.  This would be 4th and 5th (out of 9) provinces that the Mandela27 project visits through SGI-SA's efforts.  Taking the Robben Island Museum's efforts into consideration, I would not be surprised if the travelling displays get to all 9 the provinces. We at SGI-SA are particularly proud of the Mandela27 project and consider this as our flagship project – it makes a difference, it sparks discussion, it creates awareness, it enriches the lives of young South Africans and reminds old South Africans of the struggle that was all worth it!</p>
					<p>Madiba's legacy lives on!!</p>
					<div class="backtoteam bottom">
						<a href="blogs.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>					
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
