<?php

include('_header.php');

?>
	<div class="maincontainer">
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Events</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">28 aug</span><span class="year">2014</span></div>
						<h2>Coventry University's Professor of the Serious Games visits South Africa</h2>
						<p class="smalltitleorange">Vaal Triangle, South Africa</p>
					</div>
					<div class="clear"></div><br />
					<p class="short_intro">Coventry University's Professor of the Serious Games Pamela Kato visits South Africa</p>
					<p>The Mandela27 project was a highlight at the first International Conference on Serious Games (ICSG) hosted by the Serious Games Institute South Africa (SGI-SA).</p>
					<p>Legendary game designer Ernest Adams was the keynote speaker at this prestigious event that also attracted multiple industry players and academics who are active within the serious games space.</p>
					<p>As part of the Mandela27 project and the ongoing knowledge exchange between SGI-SA (from North-west University: Vaal Triangle Campus) and SGI Coventry University, Prof Kato spoke to conference delegates and SGI-SA staff about her extensive experience in the field of serious games.  Feedback from the event was extremely positive, and the SA and UK partners look forward to continuing to work together in the future.</p>
					<p><a href="http://www.sgisa.co.za/conf2014/" target="_blank">More info about the first International Conference on Serious Games.</a></p>
					<img src="files/events-icsg.jpg" />
					<div class="backtoteam bottom">
						<a href="events.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
