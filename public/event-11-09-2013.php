<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Events</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">11 sep</span><span class="year">2013</span></div>
						<h2>Project Meeting</h2>
						<p class="smalltitleorange">Stockholm, Sweden</p>						
					</div>
					<div class="clear"></div><br />				
					<p class="short_intro"> To review the first year of the project, the third partner project meeting was held on the 12th and 13th of September 2013 in Stockholm. The meeting hosted by Elderberry AB was held at the World Trade Centre in central Stockholm.</p>
					<p><img src="images/event-11-09-2013.jpg" /></p>
					<p>Mandela 27 is a two year project funded by EU Culture project, led by Coventry University Serious Games Institute with partners from Robben Island Museum SA, NWU Vaal Campus SA, TCS Digital World BE, and Elderberry AB SE. At the Project Meeting in Stockholm, the team discussed the progress of the project, reflected on lessons learned during the first year and planned the following steps. The meeting was followed up by a cultural exchange on the 14th of September amongst several of the partners, which included a tour of the Old Town of Stockholm.</p>
					<p>During the first year of the project, the development of the project elements was started. Initial research for a serious game, a small exhibition, a cultural timeline, which helped identify the needs of the target groups of young people. In parallel, the partners identified experts who contributed to the timeline of Cultural experiences, with the objective of developing user friendly and accessible online presentation. The findings are summarised in a report, reflecting the specific requirements which will form the basis for the development of the project results.</p>
					<p>Following the completion of the third meeting, the partners also carried out the internal evaluation of the project. The evaluation confirmed a high level of interest in the project’s subjects for all partner countries involved. It also highlighted the positive operative mechanisms put into practice, the achievement of the milestones set and the commitment of the partnership to successfully carry on with all foreseen activities.</p>
					<div class="backtoteam bottom">
						<a href="events.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>				
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
