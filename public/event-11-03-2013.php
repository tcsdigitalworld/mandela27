<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Events</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">11 mar</span><span class="year">2013</span></div>
						<h2>project meeting & press launch</h2>
						<p class="smalltitleorange">Cape Town, South Africa</p>						
					</div>
					<div class="clear"></div><br />				
					<p class="short_intro">For this second project meeting, the Mandela27 Team gathered in South Africa. As this is place of great inspiration for each and every one of the team, we were all very excited when we arrived in the city of Cape Town and honored, knowing that we would be spending most of our time on Robben Island, the place where Nelson Mandela was incarcerated and where history was written.</p>
					<p><img src="files/eventshome-3.jpg" /></p>
					<p>Although the focus of this second project meeting was of course on discussing concrete agreements on how to deliver high quality concepts over the next few months, The Robben Island Museum-team also scheduled visits to Jetty 1, the historical gateway to Robben Island, the Robben Island Museum Mayibuye Archive at the University of the Western Cape and of course the Robben Island Maximum Security Prison itself. Especially this last stop, and the visit to Mr. Mandela's cell in particular, made a big impression on the team. The stories of the former political prisoners gave us clear insights of the life behind the walls of this prison and although the conditions were terrible, it was astonishing to see how these brave men found hope in their comradeship. We were all moved by these stories, told with so much passion by men so convinced that they have undergone these dreadful days, months and years on Robben Island with only one goal: fighting apartheid and pursuing freedom.</p>
					<p>These visits, along with the meetings, gave us great ideas on how to move forward. We left Robben Island with a suitcase full of plans, stories and pictures and headed back to Cape Town for the South African press launch of the Mandela27 Project. We were delighted to so how the South African press warmed up to our beautiful project and we were overwhelmed by the press coverage we got in the following days.</p>
					<p>This inspiring trip and all this attention gave us even more energy to deliver a fantastic project!</p>
					<p>Check out the international press articles in the <span>Publications-section</span> to read all about the South African project presentation.</p>					
					<div class="backtoteam bottom">
						<a href="events.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>				
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
