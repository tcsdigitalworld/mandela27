<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Events</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">21 mar</span><span class="year">2014</span></div>
						<h2>21 March, International Day for the Elimination of Racial Discrimination</h2>
						<p class="smalltitleorange">Europe & South Africa</p>				
					</div>
					<div class="clear"></div><br />					
					<p class="short_intro">On this special day, UNESCO wants to recommit to ending racial discrimination and join forces to end racism. What's going on in your country on the 21<sup>st</sup>? And what can you do to raise awareness?</p>
					<p><img src="files/born-free-generation-big.jpg" alt="image" /></p>			
					<p class="white">In South Africa, the International Day for the Elimination of Racial Discrimination is simply called Human Rights Day. In this country where reconciliation led the transformation from apartheid to democracy, 21 March is a public holiday serving as both a reminder of the happenings at the Sharpeville massacre and a celebration of Mzansi's unique foundation, which gives all citizens equal rights.<br />Robben Island Museum, partnering in the Mandela27 project, will be hosting 25 schools from around Cape Town for the documentary film 'Remember Africa, Remember Sobukwe'. And of course Cape Town aka 'The Mother City' will be hosting heaps of events. Check some highlights <a  class="underlinea" target="_blank" href="http://www.capetownmagazine.com/news/Top-10-Events-this-Human-Rights-Day-in-Cape-Town/10_22_10534 ">here</a>.</p>
					<p class="white">What about Europe? Being in the heart of Europe, the Belgian capital of Brussels cannot ignore this day of rememberance. On the evening of the 21<sup>st</sup>, Brussels' City Hall will be welcoming the South African ambassador, European stakeholders, Belgian citizens and youngsters from all over the world to honor Nelson Mandela as a freedom fighter.</p>
					<p class="white">For this occasion, the Mandela27 DIY Exhibition will be set up in the impressive Gothic Room of the City Hall and Kim Vilein, project partner on behalf of TCS Digital World, will be giving a presentation on the concept and goals of the Mandela27 project.</p>
					<p class="white">The event starts at 18h30, feel free to join us in memory of Nelson Mandela:<br />
						Brussels City Hall<br />
						Gotische Zaal / Salle Gothique<br />
						Grote Markt / Grand Place<br />
						1000 Brussels<br />
						Belgium</p>					
					<div class="backtoteam bottom">
						<a href="events.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>				
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
