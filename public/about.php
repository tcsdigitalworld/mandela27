<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">The project</div>
					<img src="images/years-right.png" />
				</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="title">
						<h2>about the project</h2>
					</div>
					<p class="short_intro">Mandela27 is a cultural collaboration between the European Union and South Africa inspired by the iconic story of Nelson Mandela and the globally recognized Robben Island Museum.<br />
					The project involves:<br />
					- A physical display based upon the dimensions of Nelson Mandela's cell, housing cultural stories and works.<br />
					- An interactive cultural map of Europe and South Africa.<br />
					- A serious game depicting prison life on Robben Island.<br />
					- A panoramic tour of key spots of Robben Island Maximum Security Prison.
					</p>
					<p><img src="files/mandela_info_1.jpg" alt="image" /></p><br /><br />
					<p><strong>SUMMARY</strong><br/>Little is known in the EU about cultural events in South Africa and little is known in South Africa about EU cultural events. However most people in both regions are aware of the Nelson Mandela story in Robben Island, which is the key link in promoting intercultural dialogue.<br />
					The display based on Mandela's cell including the cultural stories will promote interest in the subject and the project across the EU.<br />
					The cultural map and the cultural serious game will support the circulation of cultural and artistic works bringing these to tens of thousands of young people in a medium that they are familiar with.</P>
					<P>The project is a true collaboration with SA partners who are supplying content and building the cultural game. This is a new industry for SA which will allow the mobility of graphic artists, cultural experts and games designers across both regions to both expand the industry and promote culture amongst young people.</p>				
					<p><strong>BACKGROUND</strong><br/>Most people are familiar with the story of Nelson Mandela and his incarceration in Robben Island. This project highlights the bleakness of Mandela's 27 years in prison contrasted with the rich, cultural heritage of SA and EU during this period.</p>
					<p><strong>AIMS</strong><br/>This project aims to foster cooperation between EU and SA based on cultural exchange. To circulate cultural and heritage items through the display and the eCulture platform and to encourage growt h in cultural games through mobility of experts in cultural display, graphic and games design.</p>					
					<p><strong>OBJECTIVES</strong><br />
						- Increase awareness of the Nelson Mandela story in EU and SA.<br />
						- Promoting culture in EU and SA.<br />
						- Creating a display based on Nelson Mandela's cell.<br />
						- Circulation of cultural events.<br />
						- Collaboration between EU and SA in development of the project and transfer of experience in cultural productions.<br />
						- Skills development through the development of a serious cultural game and a digital platform.<br />
						- Crowd sourcing people old and young to record their stories and hopes for the future.</p>
					<p><strong>SPECIAL THANKS TO:</strong></p>
					<img src="images/samsung.gif" />
					<img src="images/flyer.gif" />
					<img src="images/eu-culture.gif" />
				</div>
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
				?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
