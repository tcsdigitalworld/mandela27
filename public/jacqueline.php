<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Blogs</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">17 mar</span><span class="year">2013</span></div>
						<h2>Jacqueline, Mandela27</h2>
						<div class="clear"></div>
					</div>
					<p class="short_intro">It has been an amazing first year of the Mandela27 project. We have achieved a lot, the timeline is almost complete, the game is part finished and the exhibition is featuring in the stylish and popular BELvue museum in Belgium.</p>
					<p><img src="files/jacqueline.jpg" /></p>
					<p>We all learned about the struggle Mandela and the other freedom fighters went through to free South Africa from apartheid when we held our second project meeting at Cape Town. At the Mayibuye Archive we were moved and saddened by the censored letters home by the prisoners, and stories of separated families, too many to mention. But we were also cheered by the stories of courageous individuals in UK and Sweden who went to extraordinary lengths to assist the South African freedom fighters.</p>
					<p>The project team stayed on Robben Island for our second team meeting, it was an unforgettable experience. We left Cape Town on the boat in dusk, the lights of the harbour twinkling behind us and the boat gently rising and falling with the swell. When we arrived at Robben Island it was dark and we had some small sense of what it must have been like for the political prisoners arriving in the dark to that bleak and windy island, but they would have been cold and hungry and chained.</p>
					<p>Standing in the lime quarry on Robben Island looking at the blinding white rocks where the political prisoners spent most of their days breaking rocks surrounded by guards and dogs I was shocked and very emotional as the voices of the ex-political prisoners (who now share their experiences with visitors) resonated in my mind and I knew that this really did happen. The island was beautiful and bleak and had that air of lost opportunity and despair. The experience gave all of the project team that extra incentive to make sure that our project is successful.</p>
					<p>What struck me most during the development of the Timeline part of the project this last year were the similarities in struggles across Europe and South Africa during Nelson Mandela's incarceration, particularly in the UK where the Irish "troubles" were still fresh in my memory. "Solidarity" was also prominent in Poland where the workers struggled for freedom and the Singing Revolution in Estonia, Latvia and Lithuania where national songs were forbidden during the years of soviet occupation made a big impression on me.</p>
					<p>I am constantly amazed at how the human spirit reacts to oppression and refuses to be beaten. The project team have unearthed many localised but relatively unknown events of "culture influencing democratic change" across Europe and I can't wait until we show them to the world. </p>
					<p>This second year sees all our research come to fruition with the timeline, the exhibition is due to be displayed in Europe and South Africa soon. We are looking forward to completing the serious game about life on Robben Island, I hope that we get the message across and make even a tiny fraction of the impression upon today's generation that the fall of the Berlin wall and Mandela's walk to freedom made on me. </p>
					<div class="backtoteam bottom">
						<a href="blogs.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>					
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
