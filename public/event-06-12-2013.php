<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Events</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">6 dec</span><span class="year">2013</span></div>
						<h2>Thank you, Madiba</h2>
						<p class="smalltitleorange">Mandela27 Project Team</p>						
					</div>				
					<div class="clear"></div><br />
					<p><img src="files/mandela_info_1.jpg" alt="image" /></p>			
					<p class="white">All the partners of the EU Culture project "Mandela27" wish to express their sincere condolences to the family of Nelson Mandela and all the people of the South African nation. Nelson Mandela spent 27 years in prison, mainly at Robben Island, he was one of the founding fathers of free South Africa, an iconic  political activist commanding the respect of world leaders and an inspiration to all those working for  equality and justice. We are proud to bring his story and his struggle to the eyes of the world through this digital cultural project. We aim to use our work to contribute to his legacy.</p>
					<p class="white">Mandela27 Project Team</p>
					<div class="backtoteam bottom">
						<a href="events.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>				
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
