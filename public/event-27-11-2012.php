<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Events</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">27 nov</span><span class="year">2012</span></div>
						<h2>Project Kick-off</h2>
						<p class="smalltitleorange">Coventry, United Kingdom</p>						
					</div>				
					<div class="clear"></div><br />					
					<p class="short_intro">This was the first time all the partners had come together in one place, although some had worked together previously. Alex and Jacqueline from Coventry University had worked with Werner and Herman from North West University (Vaal Triangle) in 2011, and NWU have an existing contact with Robben Island Museum. Likewise David from Elderberry had collaborated with Nomatshayina from RIM before and also with TCS Digital World and Coventry University. So there was a great atmosphere at the first meeting.</p>
					<p><img src="files/event-27-11-2013.jpg" /></p>
					<p class="white">It was important during this meeting to set up the procedures for the project and to review the project activities for the next two years. The team from Coventry University led this discussion, with each partner in turn discussing the work packages they were responsible for. This included a review of the finances and claims procedures and the creation of a project agreement form.</p>
					<p class="white">Things got more exciting as conversation moved to the planning of the specific outputs, including the design of the physical exhibition and the digital products, and the team began to envision how the whole project would come together thematically and aesthetically. There was a real enthusiasm about the importance of the work and the ideas that were emerging from it, linking together the cultural histories of Europe and South Africa. The meeting closed with each partner having a series of actions to complete, and the team setting a date for the next meeting in Cape Town.</p>
					<p class="white">Following the meeting the partners had the opportunity to make two cultural visits. The first was to the Herbert Art Gallery in Coventry, where they viewed some of the collection, conducted press interviews and began discussion about displaying Mandela27 at the gallery. The second was to Shakespeare’s Birthplace Trust in Stratford upon Avon, where they took a tour of the site, and were shown around the literary archive that included works from South Africa linking to Shakespeare.</p>
					<p class="white">The project team appreciated being able to engage in the local cultural activities as part of the meeting as it brought new ideas and energy to their planning at this early stage of the project.</p>
					<div class="backtoteam bottom">
						<a href="events.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>				
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
