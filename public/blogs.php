<?php

include('_header.php');

?>
	<div class="maincontainer">
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Blogs</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<a href="werner.php">
						<div class="boxevents" style="background-image: url('files/blog-werner.jpg');">
							<div class="eventstitle">
								<h2>Werner, Project partner, Mandela27</h2>
								<p class="short_intro">&nbsp;</p>
							</div>
							<div class="datebox"><span class="date">19 aug</span><span class="year">2014</span></div>
						</div>
						<p class="short_intro">It appears that the world is embracing the Mandela27 display with exhibitions going up in Belgium, England, Sweden, Denmark, to name but a few. Here in South Africa it has also been a busy Madiba month. The Serious Games Institute South Africa (SGI-SA) from the North-West University: Vaal Triangle Campus was privileged to collaborate with the National Library of South Africa (NLSA) in bringing the first Mandela27 display in SA to life.  <span class="white">(more…)</span></p>
					</a>
				</div>
				<div class="content">
					<a href="jacqueline-21jul-2014.php">
						<div class="boxevents" style="background-image: url('files/homeblogs.jpg');">
							<div class="eventstitle">
								<h2>Jacqueline, Project Director,<br /> Mandela27</h2>
								<p class="short_intro">&nbsp;</p>
							</div>
							<div class="datebox"><span class="date">21 jul</span><span class="year">2014</span></div>
						</div>
						<p class="short_intro">Shortly before Mandela Day, we put a call out to schools to take up our offer of using our exhibition materials. One such school in the UK used the exhibition materials and the posters as visual aids to speak to young children aged 7 and above about Nelson Mandela.<span class="white">(more…)</span></p>
					</a>
				</div>
				<div class="content">
					<a href="jacqueline.php">
						<div class="boxevents" style="background-image: url('files/homeblogs.jpg');">
							<div class="eventstitle">
								<h2>Jacqueline, Mandela27</h2>
								<p class="short_intro">&nbsp;</p>
							</div>
							<div class="datebox"><span class="date">17 mar</span><span class="year">2013</span></div>
						</div>
						<p class="short_intro">It has been an amazing first year of the Mandela27 project. We have achieved a lot, the timeline is almost complete, the game is part finished and the exhibition is featuring in the stylish and popular BELvue museum in Belgium.<span class="white">(more…)</span></p>
					</a>
				</div>
				<div class="content">
					<a href="david.php">
						<div class="boxevents" style="background-image: url('images/blog-david.jpg');">
							<div class="eventstitle">
								<h2>David, Mandela27 teampartner from Elderberry, Sweden</h2>
								<p class="short_intro">&nbsp;</p>
							</div>
							<div class="datebox"><span class="date">9 apr</span><span class="year">2014</span></div>
						</div>
						<p class="short_intro">I think that all of the team agree that the Mandela 27 project has been one of the most interesting and creative productions we have been involved in. Whether a team member has been creating the digital material or the exhibition productions, we all seem to have put our heart and soul into the outcomes.<span class="white">(more…)</span></p>
					</a>
				</div>
				<div class="clear"></div>
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
