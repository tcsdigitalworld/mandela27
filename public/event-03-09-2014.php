<?php

include('_header.php');

?>
	<div class="maincontainer">
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Events</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">03 sep</span><span class="year">2014</span></div>
						<h2>Opening of Mandela27 Exhibition at Vallentuna Library</h2>
						<p class="smalltitleorange">Vallentuna, Sweden</p>
					</div>
					<div class="clear"></div><br />
					<p class="short_intro">Our Swedish partner, Elderberry Cultureprojects, is experienced with traditional methods for educational material as with eLearning and eCulture so it's no surprise that this Exhibition set-up in Sweden is absolutely perfect.</p>
					<p>Be sure to check this one at the the 'Vallentuna Kulturhus och Bibliotek' should you be in Sweden before the 20th of October!</p>					
					<p><a href="files/events-vallentuna-invitation.pdf">events-vallentuna-invitation.pdf</a></p>
					<img src="files/events-vallentuna.jpeg" />
					<div class="backtoteam bottom">
						<a href="events.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
