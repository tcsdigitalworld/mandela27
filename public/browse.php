<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">BROWSE THROUGH TIME</div>
					<img src="images/years-right.png" />
				</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="title">
						<h2>Discover 100 cultural events across Europe and South Africa that have led to change.</h2>
					</div>
					<p class="orange">It's important to introduce today’s youth to the milestones of democracy.<br />Therefore, 100 cultural highlights that contributed to political change have been added on to respective maps of Europe and South Africa.</p>
					<p>Historical highlights include events such as the fall of the Berlin Wall, the freedom concerts that took place all over Europe and South Africa as well as the euphoria upon the release of Nelson Mandela in 1990 and many other cultural milestones.<br />Most people are aware of these events but younger people may not realize the global impact they make.</p>
					<a class="linkbg smallerlink leftlink" href="http://highlights-in-history.mandela27.eu/#/">GO TO CULTURAL TIMELINE</a>
				</div>
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
				?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
