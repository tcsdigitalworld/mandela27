<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Add your own cultural highlight</div>
					<img src="images/years-right.png" />
				</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="title">
						<h2>You can't find your cultural highlight<br />in the Mandela27 Timeline?<br />Add it yourself!</h2>
					</div>
					<p class="orange">Today's democracy was not unfold in one day. Hundreds, even thousands of political and cultural events have led to crucial decisions and have shaped today's world.</p>
					<p>You might not have witnessed the election of Pope John Paul II or the opening event for the Channel Tunnel, but there might have been at an event celebrating that important day in history in your neighbourhood.</p>
					<p>Whether it still resonates in the back of your mind or it's just a blurry image, we would love to learn all about it!<br />Add your cultural highlight to the Mandela27 Interactive Timeline, post it on our Facebook-page or Tweet it using #Mandela27highlight.</p>
					<a class="linkbg smallerlink leftlink" target="_blank" href="http://add-your-highlights.mandela27.eu">See the cultural highlights</a>
					<a class="linkbg smallerlink rightlink" target="_blank" href="http://add-your-highlights.mandela27.eu/#/add">ADD YOUR HIGHLIGHT NOW!</a>
				</div>
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
				?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
