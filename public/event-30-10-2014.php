<?php

include('_header.php');

?>
	<div class="maincontainer">
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Events</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">30 oct</span><span class="year">2014</span></div>
						<h2>Mandela27 Project Kick-Off</h2>
						<p class="smalltitleorange">Brussels, Belgium</p>
					</div>
					<div class="clear"></div><br />
					<p class="short_intro">OPEN INVITATION – COME AND CELEBRATE THE MANDELA27 PROJECT KICK-OFF WITH US!<br />On Thursday, 30th of October we are proud to present the EU Culture project 'Mandela27' to the press and general public.</p>
					<p><img src="images/events-2-ian.jpg" /></p>
					<p>This official project presentation will be hosted by BELvue, the prestigious museum in the heart of Europe and the capital of Belgium: Brussels.</p>
					<p>You are invited to join us in celebrating the official Kick-Off of the Mandela27- EU Culture Project on Thursday, 30 October 2014 at 5 PM at the BELvue Museum in Brussels, Belgium.</p>		
					<p>We will exhibit digital and physical materials produced during the project and celebrate the collaboration between the South African and European partners who made this project successful. The exhibition materials are all freely available at <a href="http://www.mandela27.eu/">www.mandela27.eu</a></p>
					<p>Send a mail to <a href="mailto:kickoff@mandela27.eu">kickoff@mandela27.eu</a> to confirm your presence at the official Kick-Off of Mandela27. We will provide you with practical information in the weeks to come.</p>
					<p>Looking forward to welcoming you in Brussels!</p>
					<p>The Mandela27 project partners</p>
					<div class="backtoteam bottom">
						<a href="events.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>				
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
