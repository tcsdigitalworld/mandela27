<?php

include('_header.php');

?>
	<div class="maincontainer">
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">APPLY FOR A DIY KIT</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<p class="short_intro">Nelson Mandela himself said 'A good head and a good heart are always a formidable combination'. So let's get started!<br />You are only a few steps away from building your own DIY Exhibition and overwhelming your students or visitors with heaps of interesting information on the history of South African democracy.</p>
					<p><img src="files/diy-kit.jpg" /></p>
					<p>Download your own Mandela27 DIY Exhibition Kit. It's the perfect tool to visualize the evolution of democracy in schools, cultural centres and museums.</p>
					<p>The posters are now available in English, Dutch, French, Italian and Afrikaans and will soon be available in Swedish and IsiXhosa.</p>
					<a href="interview-cu-carpenter.html" class="mp4vid">
						<div class="biggerlink">
							<h3>Get tips from our carpenter!</h3>
						</div>
					</a>
					<a href="http://www.youtube.com/embed/gpjdydvC1X8" class="vid">
						<div class="biggerlink">
							<h3>The making of the DIY Exhibition at BELvue, Brussels</h3>
						</div>
					</a>
					<a href="download.php?what=buildins">
						<div class="biggerlink">
							<h3>Free Mandela27 DIY Exhibition building instructions</h3>
						</div>
					</a>
					<a href="download.php?what=posters">
						<div class="biggerlink">
							<h3>Free Mandela27 DIY Exhibition posters</h3>
						</div>
					</a>
          <a href="download.php?what=game">
            <div class="biggerlink">
              <h3>Free Mandela27 standalone Serious Game</h3>
            </div>
     		  </a>
				</div>
				<div class="clear"></div>
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
				?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
