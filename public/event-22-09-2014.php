<?php

include('_header.php');

?>
	<div class="maincontainer">
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Events</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">22 sep</span><span class="year">2014</span></div>
						<h2>Opening of Mandela27 Exhibition at Cape Town Central Library</h2>
						<p class="smalltitleorange">Cape Town, South Africa</p>
					</div>
					<div class="clear"></div><br />
					<p class="short_intro">Today, the Mandela27 Exhibition opened at Cape Town Central Library.</p>
					<p>The installation is being  only being displayed for 2 days so don’t miss out on it!</p>
					<p><img src="files/events-central-library.jpg" /></p>
					<div class="backtoteam bottom">
						<a href="events.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
