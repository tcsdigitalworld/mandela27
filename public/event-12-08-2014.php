<?php

include('_header.php');

?>
	<div class="maincontainer">
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Events</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">12 aug</span><span class="year">2014</span></div>
						<h2>Opening of Mandela27 Exhibition at Manenberg Community Library</h2>
						<p class="smalltitleorange">Manenberg, South Africa</p>
					</div>
					<div class="clear"></div><br />
					<p class="short_intro">One of our South African partners, Robben Island Museum, is building Mandela27 Exhibitions at an incredible pace!</p>
					<p>Today, they launched a new display at Manenberg Community Library. You can check it out until end of August.</p>
					<p><img src="images/manenberg.jpg" /></p>
					<div class="backtoteam bottom">
						<a href="events.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
