<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Contact</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<?php
						if(isset($_GET['succes']) && $_GET['succes'] == "contact")
						{
					?>
					<div class="title">
						<h2>Thanks for your message</h2>
					</div>
					<p class="short_intro">We will reply as soon as possible!</p>
					<?php		
						}
						else
						{
					?>
					<div class="title">
						<h2>Do you have a question?</h2>
					</div>
					<p class="short_intro">&nbsp;</p>
			        <form method="post" action="submit.php">
	                    <label for="firstname">First Name:</label><input class="inputfield" type="text" name="firstname" id="firstname" />
	                    <div class="clear"></div>

	                    <label for="lastname">Last name:</label><input class="inputfield" type="text" name="lastname" id="lastname" />
	                    <div class="clear"></div>

	                    <label for="company">E-mail:</label><input class="inputfield" type="text" name="email" id="email" />
	                    <div class="clear"></div>

	                    <label for="myquestion">Message:</label><textarea class="inputfieldbig" id="myquestion" name="myquestion"></textarea>
	                    <div class="clear"></div>

	                    <label>&nbsp;</label><input type="submit" name="submit" class="submit" value="send" />
	                    <div class="clear"></div>
			        </form>
			        <?php
			        	}
			        ?>
				</div>
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
				?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
