<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">360 Experience</div>
					<img src="images/years-right.png" />
				</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="title">
						<h2>EMBARK ON AN INTERACTIVE VISIT TO ROBBEN ISLAND</h2>
					</div>
					<p class="orange">The Mandela27 360<sup>°</sup> experience is a unique opportunity to virtually encounter some of the key locations of Robben Island prison where Nelson Mandela and other political prisoners were incarcerated for many years during the Apartheid regime in South Africa.</p>
					<P>The tour includes the cell that Mandela was held in and the corridor of cells in which it was contained.<br /> Also viewable are the prison yard where political prisoners endured hard labor and the dining room in which they took their meals.<br />Navigate through the tour using your mouse and keyboard or your touchscreen device.</P>
					<a class="biggerlink" style="display: inline-block;" href="http://e-tour.mandela27.eu">Start the 360<sup>°</sup> experience</a>
					<p>The tour is narrated by ex political prisoners and ex prison wardens, who now act as guides to the prison and its history.<br />Take a moment to listen to their testimonials before you take the tour:</p>
					<p>Ex political prisoner in prison yard
						<video width="490" height="350" poster="video/mandela27-yard-expoliticalprisoner-v.jpg" controls="controls" preload="none">
							<!-- MP4 for Safari, IE9, iPhone, iPad, Android, and Windows Phone 7 -->
							<source type="video/mp4" src="video/mandela27-yard-expoliticalprisoner.mp4" />
							<!-- Flash fallback for non-HTML5 browsers without JavaScript -->
							<object width="490" height="350" type="application/x-shockwave-flash" data="flashmediaelement.swf">
								<param name="movie" value="flashmediaelement.swf" />
								<param name="flashvars" value="controls=true&file=mandela27-yard-expoliticalprisoner.mp4" />
								<!-- Image as a last resort -->
								<img src="video/mandela27-yard-expoliticalprisoner-v.jpg" width="490" height="350" title="No video playback capabilities" />
							</object>
						</video>
					</p>
					<p>Ex political prisoner at Mandela's cell
						<video width="490" height="350" poster="video/mandela27-cells-expoliticalprisoner-v.jpg" controls="controls" preload="none">
							<!-- MP4 for Safari, IE9, iPhone, iPad, Android, and Windows Phone 7 -->
							<source type="video/mp4" src="video/mandela27-cells-expoliticalprisoner.mp4" />
							<!-- Flash fallback for non-HTML5 browsers without JavaScript -->
							<object width="490" height="350" type="application/x-shockwave-flash" data="flashmediaelement.swf">
								<param name="movie" value="flashmediaelement.swf" />
								<param name="flashvars" value="controls=true&file=mandela27-cells-expoliticalprisoner.mp4" />
								<!-- Image as a last resort -->
								<img src="video/mandela27-cells-expoliticalprisoner-v.jpg" width="490" height="350" title="No video playback capabilities" />
							</object>
						</video>
					</p>						
					<p>Ex political prisoner on boat to Robben Island
						<video width="490" height="350" poster="video/mandela27-boat-expoliticalprisoner-v.jpg" controls="controls" preload="none">
							<!-- MP4 for Safari, IE9, iPhone, iPad, Android, and Windows Phone 7 -->
							<source type="video/mp4" src="video/mandela27-boat-expoliticalprisoner.mp4" />
							<!-- Flash fallback for non-HTML5 browsers without JavaScript -->
							<object width="490" height="350" type="application/x-shockwave-flash" data="flashmediaelement.swf">
								<param name="movie" value="flashmediaelement.swf" />
								<param name="flashvars" value="controls=true&file=mandela27-boat-expoliticalprisoner.mp4" />
								<!-- Image as a last resort -->
								<img src="video/mandela27-boat-expoliticalprisoner-v.jpg" width="490" height="350" title="No video playback capabilities" />
							</object>
						</video>
					</p>						
					<p>Warden in prison yard						
						<video width="490" height="350" poster="video/mandela27-yard-warden-v.jpg" controls="controls" preload="none">
							<!-- MP4 for Safari, IE9, iPhone, iPad, Android, and Windows Phone 7 -->
							<source type="video/mp4" src="video/mandela27-yard-warden.mp4" />
							<!-- Flash fallback for non-HTML5 browsers without JavaScript -->
							<object width="490" height="350" type="application/x-shockwave-flash" data="flashmediaelement.swf">
								<param name="movie" value="flashmediaelement.swf" />
								<param name="flashvars" value="controls=true&file=mandela27-yard-warden.mp4" />
								<!-- Image as a last resort -->
								<img src="video/mandela27-yard-warden-v.jpg" width="490" height="350" title="No video playback capabilities" />
							</object>
						</video>
					</p>
					<p>Warden at Mandela's cell
						<video width="490" height="350" poster="video/mandela27-cells-warden-v.jpg" controls="controls" preload="none">
							<!-- MP4 for Safari, IE9, iPhone, iPad, Android, and Windows Phone 7 -->
							<source type="video/mp4" src="video/mandela27-cells-warden.mp4" />
							<!-- Flash fallback for non-HTML5 browsers without JavaScript -->
							<object width="490" height="350" type="application/x-shockwave-flash" data="flashmediaelement.swf">
								<param name="movie" value="flashmediaelement.swf" />
								<param name="flashvars" value="controls=true&file=mandela27-cells-warden.mp4" />
								<!-- Image as a last resort -->
								<img src="video/mandela27-cells-warden-v.jpg" width="490" height="350" title="No video playback capabilities" />
							</object>
						</video>
					</p>
					<p>Warden at dock
						<video width="490" height="350" poster="video/mandela27-dock-warden-v.jpg" controls="controls" preload="none">
							<!-- MP4 for Safari, IE9, iPhone, iPad, Android, and Windows Phone 7 -->
							<source type="video/mp4" src="video/mandela27-dock-warden.mp4" />
							<!-- Flash fallback for non-HTML5 browsers without JavaScript -->
							<object width="490" height="350" type="application/x-shockwave-flash" data="flashmediaelement.swf">
								<param name="movie" value="flashmediaelement.swf" />
								<param name="flashvars" value="controls=true&file=mandela27-dock-warden.mp4" />
								<!-- Image as a last resort -->
								<img src="video/mandela27-dock-warden-v.jpg" width="490" height="350" title="No video playback capabilities" />
							</object>
						</video>
					</p>
					<p>Warden in prison corridor
						<video width="490" height="350" poster="video/mandela27-corridor-warden-v.jpg" controls="controls" preload="none">
							<!-- MP4 for Safari, IE9, iPhone, iPad, Android, and Windows Phone 7 -->
							<source type="video/mp4" src="video/mandela27-corridor-warden.mp4" />
							<!-- Flash fallback for non-HTML5 browsers without JavaScript -->
							<object width="490" height="350" type="application/x-shockwave-flash" data="flashmediaelement.swf">
								<param name="movie" value="flashmediaelement.swf" />
								<param name="flashvars" value="controls=true&file=mandela27-corridor-warden.mp4" />
								<!-- Image as a last resort -->
								<img src="video/mandela27-corridor-warden-v.jpg" width="490" height="350" title="No video playback capabilities" />
							</object>
						</video>
					</p>
					<p>Warden in dining room
						<video width="490" height="350" poster="video/mandela27-dining-warden-v.jpg" controls="controls" preload="none">
							<!-- MP4 for Safari, IE9, iPhone, iPad, Android, and Windows Phone 7 -->
							<source type="video/mp4" src="video/mandela27-dining-warden.mp4" />
							<!-- Flash fallback for non-HTML5 browsers without JavaScript -->
							<object width="490" height="350" type="application/x-shockwave-flash" data="flashmediaelement.swf">
								<param name="movie" value="flashmediaelement.swf" />
								<param name="flashvars" value="controls=true&file=mandela27-dining-warden.mp4" />
								<!-- Image as a last resort -->
								<img src="video/mandela27-dining-warden-v.jpg" width="490" height="350" title="No video playback capabilities" />
							</object>
						</video>
					</p>
				</div>
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
				?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
