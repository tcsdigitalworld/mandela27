<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Events</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">18 jul</span><span class="year">2014</span></div>
						<h2>Opening of Madela27 Exhibition at Drakenstein Correctional Services</h2>
						<p class="smalltitleorange">Paarl, South Africa</p>					
					</div>					
					<div class="clear"></div><br />				
					<p class="short_intro">One of our South African partners, Robben Island Museum, has celebrated Mandela Day with the set-up of several Mandel27 Exhibitions. One of the many venues is Drakenstein Correctional Services.</p>
					<p><img src="files/events-drakenstein-extra1.jpg" /></p>
					<p>Drakenstein was known as Victor Verster prison.  It is 35 miles northeast of Cape Town in a town called Paarl.  This is where Mandela spend his last few years of imprisonment from December 1988 to the 11th February 1990.</p>
					<p>Members of the Western Cape Regional Works Unit and members of the Correctional Services assembled the Mandela Cell.</p>
					<p><img src="files/events-drakenstein-extra2.jpg" /></p>
					<p>Five of the six Warders from Drakenstein Correctional Services who have built the Madiba Cell Model with the help of the inmates.</p>
					<div class="backtoteam bottom">
						<a href="events.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>				
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
