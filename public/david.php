<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Blogs</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">9 apr</span><span class="year">2014</span></div>
						<h2>David, Mandela27 teampartner from Elderberry, Sweden</h2>
						<div class="clear"></div>
					</div>
					<p class="short_intro">I think that all of the team agree that the Mandela 27 project has been one of the most interesting and creative productions we have been involved in. Whether a team member has been creating the digital material or the exhibition productions, we all seem to have put our heart and soul into the outcomes.</p>
					<p><img src="images/blog-david.jpg" /></p>
					<p>I think that all of the team agree that the Mandela 27 project has been one of the most interesting and creative productions we have been involved in. Whether a team member has been creating the digital material or the exhibition productions, we all seem to have put our heart and soul into the outcomes. And who wouldn’t, given the nature of the story we have to tell?</p>
					<p>Telling the story about the struggle Mandela and the other freedom fighters went through to free South Africa from apartheid and balancing it with cultural events that occurred in other part of the world has given a fascinating twist. Complex and intriguing.</p>
					<p>After a final push over the Christmas holiday the first live exhibition opened at BELvue museum in central Brussels. It was an honour to open at this prestigious location and the exhibition fitted well with the main theme of the museum which is democracy. The display with its cheap building and DIY feel material contrasted well with neo classical splendor of the museum. Visitors remarked they could really get a feel for the space of the prison cell and develop a sense of poignancy and respect for prisoners who spent years of their lives confined so. Simple and effective.</p>
					<p>The Exhibition was then invited to Brussels City Hall for the yearly celebration of the International Day for the Elimination of Racial Discrimination. On this day, UNESCO wants to recommit to ending racial discrimination and join forces to end racism. Delegates of the South African Embassy, European stakeholders, Belgian citizens and youngsters from all over the world honoured Nelson Mandela as a freedom fighter and shared their stories about apartheid.</p>
					<p>Next on the planning are The Herbert Museum &amp; Gallery in Coventry and Bozar Museum of Fine Arts in Belgium. We are looking forward to integrating the digital material fully into the display. The Serious Game and the Cultural Timeline are looking impressive, so we can’t wait!</p>
					<p>A whole range of venues are now being added to our list in Sweden, Denmark and Italy as well as South Africa. If you are interested in bringing it to avenue near you, just contact us!</p>
					<div class="backtoteam bottom">
						<a href="blogs.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>					
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
