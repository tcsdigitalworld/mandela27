<?php

include('_header.php');

?>
	<div class="maincontainer">
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">PUBLICATIONS</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">			
				<div class="content">				
					<div class="newstitle">
						<div class="datecontainer"><span class="date">8 july</span><span class="year">2013</span></div>
						<h2>What Mandela27 does for youngsters</h2>
						<p class="smalltitleorange">Brussels, Belgium</p>
					</div>
					<div class="clear"></div><br />
						<p class="short_intro">Mandela27 examines how Culture has brought about change in Europe and South Africa through the lens of Nelson Mandela's struggle against apartheid.</p>
						<p class="white">Mandela27 represents a 2-year cultural collaboration between the European Union (Culture Program) and South Africa, and aims to introduce the younger generation to the cultural and political developments that have shaped both South Africa and Europe into what they are today. The core focus includes the struggle against apartheid and the role that Nelson Mandela played in orchestrating political change.</p>
						<p class="white">A towering figure in the 20th century and Nobel Laureate, Nelson Mandela dedicated his life to the quest for a democratic South Africa. After 27-years of incarceration he became the epitome of courage and perseverance in the fight against the South African apartheid-regime. At the age of 94 the elderly statesman has been prominent in news broadcasts on an almost daily basis due to his ailing health - much to the sorrow of the entire world. Young people from around the world are now asking themselves who Nelson Mandela is and what his legacy means to humanity, for this reason the Mandela27 project is of paramount importance.</p>
						<p class="white">In order to achieve this in a way that is attractive and engaging, the project consists of several interactive concepts:</p>
						<p class="white">DIY EXHIBITION</p>
						<p class="white">The DIY Exhibition is a low-cost, easy-to-assemble display to be used for in schools and other cultural institutions. The exhibit depicts a replica (with the exact floor span) of Nelson Mandela's prison cell at Robben Island with exterior artwork showing the chronological progression from the inception of apartheid to the freedom of the people. Institutions or organisations throughout the world wishing to house/display this exhibit will be able to download the DIY kit and documentation via the project portal www.mandela27.eu from early 2014. To date the DIY Exhibition has already been requested by museums in Great Britain, South Africa, Belgium and Sweden.</p>	
						<p class="white">SERIOUS GAME</p>
						<p class="white">Leading serious game development North-West University - Vaal Triangle Campus (NWU Vaal) and Coventry University in England, will join forces to create an interactive graphic novel for the purposes of the project. Web portal visitors will be able to access the game and by means of a digital and interactive graphic novel, actively gain an impression of what life as a political prisoner in Robben Island was like. The graphic novel will be available at www.mandela27.eu in the last quarter of 2013.</p>
						<p class="white">CULTURAL MAP</p>
						<p class="white">The third element of the Mandela27 project is the development of an interactive cultural time line. In order to introduce today's youth to the milestones of democracy, 100 cultural highlights that contributed to political change have been added on to respective maps of Europe and South Africa. Historical highlights include events such as the fall of the Berlin Wall, the freedom concerts that took place all over Europe and South Africa as well as the euphoria upon the release of Nelson Mandela in 1990. Most people are aware of these events but younger people may not realize the global impact they make. This interactive timeline will be published on www.mandela27.eu at the end of 2013.</p>
						<p class="white">Mandela27 has been made possible through the support of the Culture Program of the European Union. The project represents a dynamic collaboration between Coventry University in the UK, Robben Island Museum and North West University: Vaal Triangle Campus in South Africa, Elderberry Culture Projects in Sweden and TCS Digital World in Belgium.</p>
						<p class="short_intro text-right">Kim, Mandela27</p>
									
					
				</div>						
				<div class="clear"></div>
		
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
