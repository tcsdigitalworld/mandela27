<?php

include('_header.php');

?>
	<div class="maincontainer">
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">SERIOUS GAME</div>
					<img src="images/years-right.png" />
				</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="title">
						<h2>EXPERIENCE THE LIFE AS A POLITICAL PRISONER AT ROBBEN ISLAND</h2>
					</div>
					<p class="orange">Mandela27 created an interactive graphic novel showing what life for political prisoners was like at Robben Island Prison.</p>
					<p class="orange">Take part in this truly moving story and experience what prisoners, wardens and families went through during apartheid.</p>
					<p><a href="serious_game_detail.php">(more...)</a></p>
					<div class="img">
						<a href="serious_game_detail.php"><img src="files/seriousgame_3.jpg" alt="image" /></a>
						<p class="orange center">serious game</p>
					</div>
					<p><a class="linkbg smallerlink game" style="display:block" href="game/index.html">Play the game online</a></p>
					<p><a onclick="return conf();" class="linkbg smallerlink" style="display:block" href="game/M27_Win_Final.zip">Download game for PC</a></p>
					<p><a onclick="return conf();" class="linkbg smallerlink" style="display:block" href="game/M27_Mac_Final.zip">Download game for MAC</a></p>
					<p><a onclick="return conf();" class="linkbg smallerlink" style="display:block" href="game/Mandela27.apk">Download game on your mobile</a></p>
				</div>
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
				?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<script type="text/javascript">
		function conf() {
			return confirm('WARNING!\n\nDownloading the Serious Game will take app. 40MB of disk space.\nAre you sure you want to save these files?');
		}
	</script>
<?php include('_footer.php'); ?>