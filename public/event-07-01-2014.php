<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Events</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">7 jan</span><span class="year">2014</span></div>
						<h2>Opening of first DIY Exhibition</h2>
						<p class="smalltitleorange">Brussels, Belgium</p>						
					</div>
					<div class="clear"></div><br />				
					<p class="short_intro">'It always seems impossible until it's done.' is one of Mr Mandela's most memorable quotes. Bearing that in mind, we gathered at the prestigious BELvue Museum in Brussels to kick off the new year with the construction of the very first DIY Exhibition.</p>
					<a href="http://www.youtube.com/embed/gpjdydvC1X8" class="vid cboxElement">
						<div class="biggerlink">
							<h3>The making of the DIY Exhibition at BELvue, Brussels</h3>
						</div>
					</a>
					<p class="white">One of the first concepts that was introduced by David at Elderberry Culture Projects before the Mandela27 project was even launched was to re-build an exact copy of Nelson Mandela's cell on Robben Island. The entire team immediately agreed that the cell should be developed to become a powerful exhibition, guiding people through the recent history of South Africa.</p>
					<p class="white">As cultural exchange is one of the main purposes of Mandela27, we designed a concept that is easy to set-up in every school, museum, gallery and by any organisation that is willing to work around Nelson Mandela the themes that are linked to our project. We named it 'DIY Exhibition'  hoping it would convince organisations in Europe as well as South Africa to really DIY it. But before we put the manual online, we decided to launch the DIY Exhibition ourselves at BELvue as a 'general rehearsal'.</p>
					<p class="white">We contacted Santosh -an experienced carpenter- and were pleasantly surprised to see how he managed to easily select the perfect components and assemble the right parts based on our building instructions and cutting lists. The display was up in barely one hour. Simple props like a bench, a felt sleeping mat and a bucket transformed the abstract wooden construction into a humble prison cell and 10 posters on the outer walls of the cell tell the story of apartheid from the beginning of colonialism until the triumph of democracy.</p>
					<p><img src="files/2-Comparison.png" alt="image" /></p>
					<p><img src="files/5-Comparison.png" alt="image" /></p>
					<p class="white">We knew all along what we wanted to reflect with our DIY Exhibition but is was only until it was set up at BELvue that we realised just how much it impacted visitors and even team members. Comments from the staff at BELvue were that it made people realise that Mandela was in that small space for 27years and they also learned about apartheid and the struggle for freedom in a very touching manner.</p>
					<a href="http://mandela27.eu/diy_kit.php">The Mandela27 DIY Kit is now available.</a>

					
					<div class="backtoteam bottom">
						<a href="events.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>				
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
