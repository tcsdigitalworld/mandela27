<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Free Mandela27 DIY Exhibition building instructions</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<?php
						if(isset($_GET['succes']) && $_GET['succes'] == "contact")
						{
					?>
					<div class="title">
						<h2>Thanks for your message</h2>
					</div>
					<p class="short_intro">We will reply as soon as possible!</p>
					<?php		
						}
						else
						{
					?>
					<div class="title">
						<h2>Thank you for downloading the Mandela27 Exhibition building instructions.</h2>
						<p>Please fill in this application form to be redirected to the download page:</p>
					</div>
					<p class="short_intro">&nbsp;</p>
			        <form method="post" action="submit.php">
	                    <label for="firstname">First Name:</label><input class="inputfield" type="text" name="firstname" id="firstname" />
	                    <div class="clear"></div>

	                    <label for="lastname">Last name:</label><input class="inputfield" type="text" name="lastname" id="lastname" />
	                    <div class="clear"></div>

	                    <label for="function">Function:</label><input class="inputfield" type="text" name="function" id="function" />
	                    <div class="clear"></div>

	                    <label for="company">E-mail:</label><input class="inputfield" type="text" name="email" id="email" />
	                    <div class="clear"></div>

	                    <label name="typeofvenue">Type of venue:</label>
	                    <select class="inputfielddrop" name="typeofvenue" id="typeofvenue">
	                    	<option>School</option>
	                    	<option>Gallery</option>
	                    	<option>Museum</option>
	                    	<option>Cultural centre</option>
	                    	<option>Other</option> 
	                    </select>
	                    <div class="clear"></div>

	                    <label for="namevenue">Name of venue:</label><input class="inputfield" type="text" name="namevenue" id="namevenue" />
	                    <div class="clear"></div>

	                    <label for="countryvenue">Country of venue:</label><input class="inputfield" type="text" name="countryvenue" id="countryvenue" />
	                    <div class="clear"></div>

	                    <a href=""><img src="" /></a>
	                    
			        </form>
			        <?php
			        	}
			        ?>
				</div>
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
				?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
