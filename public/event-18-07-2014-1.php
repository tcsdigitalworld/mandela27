<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Events</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">18 jul</span><span class="year">2014</span></div>
						<h2>Opening of Madela27 Exhibition at the National Library of South Africa</h2>
						<p class="smalltitleorange">Pretoria, South Africa</p>					
					</div>					
					<div class="clear"></div><br />				
					<p class="short_intro">North West University’s part in the Mandela27 project was officially launched at the National Library of South Africa on Mandela Day and instantly reached a highpoint when thousands of people visited the exhibition.</p>
					<p><img src="files/events-national-library-extra1.jpg" /></p>
					<p>Other than teaching and research, NWU also plays an important role in a region's business activity. SGI-SA is continuously involved in commercial projects, bringing the latest EGBL and Mobile solutions to clients in a myriad of sectors and industries ranging from banking, health and right through to heavy engineering.</p>
					<p>These tools come in the form of reusable technologies (adaptable to many scenarios) or as bespoke interventions that address key concepts and situations unique to the subject company. The Mandela27 Serious Game is a perfect example of this strategy.</p>
					<p><img src="files/events-national-library-extra2.jpg" /></p>
					<div class="backtoteam bottom">
						<a href="events.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>				
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
