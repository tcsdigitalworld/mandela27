<?php

include('_header.php');

?>
	<div class="maincontainer">
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Blogs</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">21 jul</span><span class="year">2014</span></div>
						<h2>Jacqueline, Project Director, Mandela27</h2>
						<div class="clear"></div>
					</div>
					<p class="short_intro">Shortly before Mandela Day, we put a call out to schools to take up our offer of using our exhibition materials. One such school in the UK used the exhibition materials and the posters as visual aids to speak to young children aged 7 and above about Nelson Mandela.</p>
					<p>The posters were a good prompt to discuss very difficult subject matters such as; slavery, racism and violence. The children found it hard to understand apartheid but when their teacher asked them how they would feel if she told the black children to sit on the floor and the white children to sit on the stairs they were outraged with shouts of "No, Miss that's not fair!" this is a small example of how the Mandela27 exhibition is bringing difficult subjects into the light.</p>
					<div class="backtoteam bottom">
						<a href="blogs.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>					
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
