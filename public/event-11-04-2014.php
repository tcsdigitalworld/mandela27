<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Events</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">11 apr</span><span class="year">2014</span></div>
						<h2>Opening of Mandela27 Exhibtion at The Herbert Gallery and Museum in UK</h2>
						<p class="smalltitleorange">Coventry, UK</p>
					</div>					
					<div class="clear"></div><br />				
					<p class="short_intro">We officially launched Mandela27 in the UK at the Herbert Gallery and Museum in Coventry!</p>
					<p>Watch Alex Woolner’s interview with BBC TV here!</p>
					<p><iframe width="484" height="272" src="//www.youtube.com/embed/kcOkVO85C-A?rel=0&showinfo=0&autohide=1" frameborder="0" allowfullscreen></iframe></p>
					<p>The Mandela27 Exhibition was officially opened with a private view at which we had speeches from the Director of the Gallery, Kevin Warwick (the deputy vice chancellor for research at Coventry University) and Jacqueline Cawston, Mandela27’s project leader.</p>
					<p>The exhibition has been warmly received so far and we have received press interest from BBC TV, radio and online.</p>
					<div class="backtoteam bottom">
						<a href="events.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>				
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
