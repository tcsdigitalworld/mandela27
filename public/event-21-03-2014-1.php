<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Events</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">21 mar</span><span class="year">2014</span></div>
						<h2>Human Rights Day Celebration</h2>
						<p class="smalltitleorange">Brussels, Belgium</p>
					</div>					
					<div class="clear"></div><br />				
					<p class="short_intro">Human Rights Day is officially called ‘International Day for the Elimination of Racial Discrimination’. On this special day, UNESCO wants to recommit to ending racial discrimination and join forces to end racism.</p>
					<p><img src="files/events-human-rights-day-extra.jpg" /></p>
					<p>For this occasion, the Mandela27 Exhibition was set up at the Brussels' City Hall. The audience was amazing and consisted of youngsters, politicians, ambassadors and elderly people of many different nationalities. As the Congo is a former Belgian colony, there was an impressive involvement of the (South-)African community. We got a lot of people passing by to report on their experiences during apartheid.</p>
					<p>Kim Vilein, project partner on behalf of TCS Digital World, gave a presentation on the concept and goals of the Mandela27 project. The University of Ghent sent a PHD from the Faculty of Arts and Philosophy specialising in African Languages and Cultures and the event closed with a fantastic, colorful fashion show!</p>
					<div class="backtoteam bottom">
						<a href="events.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>				
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
