<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">What & why?</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<h2>BUILD YOUR OWN REPLICA OF NELSON MANDELA'S CELL ON ROBBEN ISLAND</h2><br />
					<p class="short_intro">The Mandela27 DIY Exhibition is a low-cost, easy-to-assemble and highly educational display for museums, galleries, schools & colleges.<br />
						The exhibit depicts a replica (with the exact floor span) of Nelson Mandela's prison cell at Robben Island with exterior artwork showing the chronological progression from the inception of apartheid to the freedom of the people.<br />
						Institutions or organizations throughout the world wishing to house/display this exhibit are welcome to download the DIY kit and documentation from our website.<br />
						To date the DIY Exhibition has already been requested by BELvue Museum in Brussels, Belgium, The Herbert Art Gallery & Museum in Coventry, Great Britain and Robben Island Museum in Cape Town, South-Africa. Besides, tens of schools, libraries and cultural organisations in Europe, Africa and even the US have downloaded the deliverables!</p>
					<p><img src="files/mandela27.eu_whatwhy.jpg" /></p>
					<p>WHY?<br />
						Young people from around the world should know who Nelson Mandela was and what his legacy meant to humanity, for this reason the Mandela27 project is of paramount importance.</p>
					<p>The Mandela27 DIY Exhibition is an essential tool to bring his story in an attractive and engaging way.<br />
						Youngsters will be introduced to the small cells, critical conditions and racist management within the walls of Robben Island as well also the struggle against apartheid, powerlessness and riots outside the prison walls, in South Africa all over the world.</p><br />
					<p>WHAT?<br />
						The Mandela27 DIY Exhibition follow the dimensions of Nelson Mandela's cell on Robben island and can be constructed using cheap, standard wood and plywood.</p>
					<p>The interior is bare except for the bedding material, a bench and a plate and cup that accompanied the prisoners in their cell.</p>
					<img src="files/what-&-why-2.png" />
					<p>On the exterior, ten posters, containing drawings designed by students at North-West University, Vaal Triangle Campus can be mounted to the walls.<br />
						These sketches follow a graphic novel style together and tell the story of the struggle for democracy using valuable material from UWC-Robben Island Museum Mayibuye Archives -which holds the Robben Island records and many of the records from organisations engaged in the struggle against apartheid.</p>
					<p>These 10 posters raise the following topics:</p>
					<p>
						- Introduction to Mandela27<br />
						- Colonialism and slavery<br />
						- What is apartheid?<br />
						- Defiance: & Soweto<br />
						- Fight for democracy<br />
						- Robben Island<br />
						- Mandela and the political prisoners<br />
						- Don't entertain Apartheid!<br />
						- How culture can influence change in society<br />
						- Culture, reconciliation and citizenship
					</p><br />
					<p>HOW?<br />
						The Mandela27 DIY Exhibition can be constructed following downloadable building instructions.<br />
						The 10 posters are downloadable as well for builders of the DIY Exhibition with printing equipment. Those without printing equipment can order a set of posters from the Mandela27 teampartners in Europe and South Africa.
					</p>
					<p>A series of educational resources for teachers and pupils/students will follow the DIY Exhibition and are also downloadable here.</p>
					<a href="interview-cu-carpenter.html" class="mp4vid">
						<div class="biggerlink">
							<h3>Get tips from our carpenter!</h3>
						</div>
					</a>
					<a href="http://www.youtube.com/embed/gpjdydvC1X8" class="vid">
						<div class="biggerlink">
							<h3>The Making of the DIY Exhibition at BELvue, Brussels</h3>
						</div>
					</a>

				</div>
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
				?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
