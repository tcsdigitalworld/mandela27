<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="../images/years-left.png" />
					<div id="text">About us</div>
					<img src="../images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="title">
						<h2>about the project</h2>
					</div>
					<div class="backtoteam">
						<a href="../about_us.php"><img src="../images/arrow_left_small.png" alt="image" />Back to all partners</a>
					</div>
				</div>
				<div id="about_picture_container">
					<div id="about_picture">
						<a href="http://www.nwu.ac.za/nwu/index.html" target="_blank"><img src="../files/logo-robbenisland.jpg" alt="image" /></a>
					</div>
				</div>
				<div class="clear"></div>
				<div class="content">
					<p>
						RobbenIsland is a cultural heritage museum, located in Table Bay, about 6.9 km west of Cape Town. The word Robben is derived from the Dutch word Seal, &quot;Seal Island&quot;. 
					</p>
					
					<p>
						For nearly 400 years, Robben Island was used as a place of banishment ,imprisonment, and isolation for all those who were deemed to be social misfits.</p>
					
					<p>
						From the first incarceration of Chie fAutshumato in 1632; the lepers and lunatics that were locked up in the 17th century until the 20th century;  To Makhanda kaNxele - a legendary Xhosa prophet, who was imprisoned in 1820. And the most infamous political inmates Nelson Mandela, Walter Sisulu, Govan Mbeki, Ahmad Kathrada, Andrew Mlangeni and other scores of freedom fighters who fought against Apartheid. 
					In 1997, Robben Island was declared a cultural heritage museum and was declared a World Heritage Site by UNESCO in 1999. 
					</p>
					
					<p>
						As a World Heritage Site, the museum provides a unique learning environment because of its rich historical past and natural environment. The museum is committed to making sure that not just the history of Robben Island is remembered and promoted, but that also its unique symbol of triumph of the human spirit over hardship and injustice is promoted and understood across the world. The museum consists of four main sites namely:
					</p>
					
					<ul>
						<li>1. The Island (a World Heritage Site);</li>
						<li>2. Jetty 1 used as the gateway to the island and the Robben Island Maximum Security Prison, holding cells.</li>
						<li>3. The Nelson Mandela Gateway, currently gate way to the island, built late 90's.</li>
						<li>4. Robben Island UWC Mayibuye Archives situated at the University of the Western Cape.</li>
					<ul>
				<div class="backtoteam bottom">
					<a href="../about_us.php"><img src="../images/arrow_left_small.png" alt="image" />Back to all partners</a>
				</div>
				</div>
			</div>
			<div id="right">
				<?php
					include('../_keepintouch.php');
				?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
