<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="../images/years-left.png" />
					<div id="text">About us</div>
					<img src="../images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="title">
						<h2>about the project</h2>
					</div>
					<div class="backtoteam">
						<a href="../about_us.php"><img src="../images/arrow_left_small.png" alt="image" />Back to all partners</a>
					</div>
				</div>
				<div id="about_picture_container">
					<div id="about_picture">
						<a href="http://www.tcsdigitalworld.com/" target="_blank"><img src="../files/logo-tcsdw.jpg" alt="image" /></a>
					</div>
				</div>
				<div class="clear"></div>
				<div class="content">
					<p>Founded in 2005 TCS Digital World is a hybrid company bridging agency knowledge with deep IT knowledge. The classic marketing and communication approach, television, radio and print need to expand and embrace digital, taking it from pure web to mobile. This will take time, as it is an enormous change in an industry that is more conservative as one would imagine. The team of TCS Digital World is represented by a set of multidisciplinary experts from the area of computer science, engineering, communications, marketing, art and social sciences. We focus on Mobile Development, NFC, Bluetooth, Location Based Services, Contextual Awareness, predictive web etc. TCS Digital World sees in the future connected screens, from TV to smartphones and everything in between, with an easy way of authentication, and always desktop ready.Too blurry? Just remember that anyones online dream could be our daily passion.</p>
					<p>Contact <a class="underlinea" href="mailto:kimv@tcsdigitalworld.com">Kim Vilein</a> at TCS Digital World for more information about the digital and physical cultural learning tools.</p>

					<div class="backtoteam bottom">
					<a href="../about_us.php"><img src="../images/arrow_left_small.png" alt="image" />Back to all partners</a>
				</div>
				</div>
			</div>
			<div id="right">
				<?php
					include('../_keepintouch.php');
				?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
