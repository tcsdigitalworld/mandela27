<!DOCTYPE html>
<html>
<head>
	<link href='http://fonts.googleapis.com/css?family=Amatic+SC:400,700' rel='stylesheet' type='text/css'>
	
	<title></title>

	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" user-scalable="no" />
	<meta name="description" content="" />
	<meta property="og:image" content="http://www.mandela27.eu/images/mandela.jpg" />
	<meta property="og:title" content="Mandela27" />
	<meta property="og:description"content="A cultural exchange project between South Africa and the EU."/>
	
	<link rel="stylesheet/less" href="../css/style.less">
	<script src="../js/less.js"></script>
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<script src="../js/jquery.bxslider/jquery.bxslider.min.js"></script>	
	<link rel="stylesheet" href="../css/jquery.bxslider.css">
	
	<script>
	$(document).ready(function(){
		$('.bxslider').bxSlider({
			touchEnabled:false,
			controls: false
		});
	});
	</script>
	
	<script type="text/javascript">
	
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-41645221-1']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	
	</script>

	
</head>

<body>
	<div id="header">
		<div class="maincontainer">
			<div id="menubar">
				<a href="/"><h1>Mandela 27</h1></a>
				<ul id="nav" class="drop">
					<li class="l1">About
						<ul>
							<li><a href="../about.php">The project</a></li>
							<li><a href="../about_us.php">The team</a></li>
							<li><a href="../project_events.php">Project Events</a></li>
						</ul>
					</li>
					
					<li class="l1">News
						<ul>
							<!--<li><a href="#">Articles</a></li>-->
							<li><a href="../events.php">Events</a></li>
							<li><a href="../publication.php">Publications</a></li>
							<li><a href="../blogs.php">Blogs</a></li>
							<li><a href="../newsletters.php">Newsletters</a></li>
						</ul>
					</li>
					<li class="l1">DIY Exhibition
						<ul>
							<li><a href="../whatwhy.php">What & why?</a></li>
							<li><a href="../diy_kit.php">Apply for a diy-kit</a></li>
							<li><a href="../teacher_notes.php">Teacher notes</a></li>
							<li><a href="../exhibitions.php">Exhibitions near you</a></li>
						</ul>
					</li>
					<li class="l1"><a href="../serious_game.php">Serious game</a></li>
					<li class="l1">Cultural Timeline
						<ul>
							<li><a href="../browse.php">Browse through time</a></li>
							<li><a href="../add_highlight.php">Add your own cultural highlight</a></li>							
						</ul>
					</li>
					<li class="l1"><a href="../experience.php">360 experience</a></li>
					<li class="l1"><a href="../contact.php">Contact</a></li>
				</ul>
				<div class="clear"></div>
			</div>
			<div>
				
			</div>
		</div>
	</div>