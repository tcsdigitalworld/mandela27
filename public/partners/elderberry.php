<?php
include('_header.php');
?>
	<div class="maincontainer">
		<div class="pagetitle">
				<div class="cen">
					<img src="../images/years-left.png" />
					<div id="text">About us</div>
					<img src="../images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="title">
						<h2>about the project</h2>
					</div>
					<div class="backtoteam">
						<a href="../about_us.php"><img src="../images/arrow_left_small.png" alt="image" />Back to all partners</a>
					</div>
				</div>
				<div id="about_picture_container">
					<div id="about_picture">
						<a href="http://www.elderberry.nu/" target="_blank"><img src="../files/logo-elderberry.jpg" alt="image" /></a>
					</div>
				</div>
				<div class="clear"></div>
				<div class="content">
					<p>Elderberry AB undertakes educational development and strategic studies, authoring, editing and publishing, within youth, adult, teacher, heritage and vocational education.  Our work often involves socio-cultural and urban implications. The company is experienced with traditional methods for educational material as with eLearning and eCulture. The company has run the EU sponsored teacher training courses for last 9 years, training over 1000 teachers in one week courses on museum and heritage education, held in Stockholm, Istanbul, Florence, Lesbos and Dublin <a href="www.eucourses.eu" target="_blank">www.eucourses.eu</a>. Elderberry also curates and produces small educational exhibitions as part of our portfolio.
 </p>
				<div class="backtoteam bottom">
					<a href="../about_us.php"><img src="../images/arrow_left_small.png" alt="image" />Back to all partners</a>
				</div>
				</div>
			</div>
			<div id="right">
				<?php
					include('../_keepintouch.php');
				?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
