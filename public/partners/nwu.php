<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="../images/years-left.png" />
					<div id="text">About us</div>
					<img src="../images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="title">
						<h2>about the project</h2>
					</div>
					<div class="backtoteam">
						<a href="../about_us.php"><img src="../images/arrow_left_small.png" alt="image" />Back to all partners</a>
					</div>
				</div>
				<div id="about_picture_container">
					<div id="about_picture">
						<a href="http://www.nwu.ac.za/nwu/index.html" target="_blank"><img src="../files/logo-northwestun.jpg" alt="image" /></a>
					</div>
				</div>
				<div class="clear"></div>
				<div class="content">
					<p>Located on the banks of the Vaal River in a proclaimed nature reserve, the Vaal Triangle Campus is an inspiring setting for work and play. Campus academics are motivated and well qualified, student success rates above average, and the research, sport, student support and cultural facilities outstanding.  As a university, our core business is teaching and learning with an offering of a range of qualifications from pregraduate (BSc, BCom, BA and BEd) through to PhD level in each of these degree studies.  The NWU: Vaal Triangle Campus also places a strong emphasis on research outputs in the form of journal articles, conference papers, books and more.</p>
					<p>Although the Vaal Triangle Campus is the smallest of the NWU's three campuses, it experienced record growth in the past few years. In 2012, there were 6 100 students. The average pass rate for all modules presented on the Campus was 82,4%, which is higher than the norm set by South Africa's Department of Education.</p>
					<p>One reason for the Campus's consistently strong academic performance is the skills profile of its academic personnel. In all, 84% of its lecturers already have master's or doctoral degrees, and 30% are busy with postgraduate studies.</p>
					<p>The Faculty of Economic Sciences and Information Technology aspire to be the preferred choice for business and IT studies. By providing innovative and relevant teaching and learning as well as research our graduates make a positive impact in their chosen sector. These programmes, which address various domains of academic enquiry, are geared to provide solutions to real-life situations within a vibrant economy.  In order to facilitate being the preferred choice for business and IT studies, the Faculty has successfully brought about two integral initiatives.  Firstly, a business incubator to provide a platform for entrepreneurship and in turn to assist with the local economic development of our region has been established.  Most recently, the Faculty has also brought about a Serious Games Institute that is all about delivering immersive media, ranging from basic Adobe Flash animations to immersive and code-driven 3D environments, for non-leisure activities.
 </p>
				<div class="backtoteam bottom">
					<a href="../about_us.php"><img src="../images/arrow_left_small.png" alt="image" />Back to all partners</a>
				</div>
				</div>
			</div>
			<div id="right">
				<?php
					include('../_keepintouch.php');
				?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
