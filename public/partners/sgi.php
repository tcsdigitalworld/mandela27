<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="../images/years-left.png" />
					<div id="text">About us</div>
					<img src="../images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="title">
						<h2>about the project</h2>
					</div>
					<div class="backtoteam">
						<a href="../about_us.php"><img src="../images/arrow_left_small.png" alt="image" />Back to all partners</a>
					</div>
				</div>
				<div id="about_picture_container">
					<div id="about_picture">
						<a href="http://www.seriousgamesinstitute.co.uk/" target="_blank"><img src="../files/logo-coventry.png" alt="image" /></a>
					</div>
				</div>
				<div class="clear"></div>
				<div class="content">
					<p>Coventry University (CU) Serious Games Institute (SGI) is a centre of excellence for digital media innovation working across all sectors including cultural and heritage projects. The Institute combines technical expertise with a high level of applied research and project management. Coventry University has a reputation for working with business and non-profit organisations and runs over 300 projects in any one year including Leonardo, Transversal and Daphne. The Institute is currently working with the Shakespeare Birthplace Trust, the Herbert Gallery and other cultural and heritage groups that require digital outlets and new ways of using cultural media. The SGI is a global digital leader working in game based technology for PC and mobile, augmented reality and brain computer interfaces.</p>
					<p>Contact <a class="underlinea" href="mailto:AWoolner@cad.coventry.ac.uk">Alex Woolner</a> or <a class="underlinea" href="mailto:JCawston@cad.coventry.ac.uk">Jacqueline Cawston</a> at Coventry University &mdash; Serious Games Institute for more information about the digital and physical cultural learning tools.</p>
					<div class="backtoteam bottom">
						<a href="../about_us.php"><img src="../images/arrow_left_small.png" alt="image" />Back to all partners</a>
					</div>
				</div>
			</div>
			<div id="right">
				<?php
					include('../_keepintouch.php');
				?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
