<?php

include('_header.php');

?>
	<div class="maincontainer">
		<div id="linkbar">
			<div id="byeu">
				<p>An initiative of the	<br/>European Commission</p>
				<a href="#"><img src="images/eu_culture.jpg" alt="EU Culture" /></a>
			</div>
			<div id="socialmedia_links">
				<ul>
					<!-- <li><a href="#"><img src="images/rss_round.png" alt="rss" /></a></li> -->
					<li><a href="https://www.facebook.com/mandela27" target="_blank"><img src="images/facebook_round.png" alt="facebook" /></a></li>
					<li><a href="https://twitter.com/Mandela27EU" target="_blank"><img src="images/twitter_round.png" alt="twitter" /></a></li>
				</ul>
			
			</div>
			<div class="clear"></div>
		</div>
		<div id="content" class="home">
			<div id="left">
				<div id="main_article">
					<img src="/images/mandela.jpg" alt="home main article" />
					<div class="clear"></div>
				</div>
				<div id="events">
					<a href="event_launch.php">
						<p class="date">11-14 MAR</p>
						<p class="title">Report of Project Meeting</p>
						<p class="location">Robben Island & Cape Town</p>
						<div class="clear"></div>
					</a>
				</div>
				<div id="about">
					<h2>About us</h2>
					<p>Mandela27 is a cultural collaboration between the European Union and South Africa inspired by the iconic story of Nelson Mandela and the globally recognized Robben Island Museum.</p>
<p>The project involves a physical display based upon the dimensions of Nelson Mandela’s cell, housing cultural stories and works. Plus digital outputs that include an interactive cultural map of Europe and SA; a serious game depicting prison life on Robben Island; and a panoramic tour of key spots of Robben Island Maximum Security Prison.
	</p>
				<a class="readmore" href="/about.php"><img src="/images/more.png" alt="Read more"/></a>
				</div>
			</div>
			<div id="right">
				<div id="slider">
					<ul class="bxslider">
						<li><img src="/images/home_carousel/1.jpg" alt="img" /></li>
						<li><img src="/images/home_carousel/1.jpg" alt="img" /></li>
					</ul>
					<div class="clear"></div>
				</div>	
				<div id="social_media">
					<ul>
						<li>
							<img class="icon" src="/images/facebook.png" />
							<p class="text">Latest news on the project from our partners from the <a href="http://covresearch.wordpress.com/2013/03/01/serious-games-institute-mandela-27/" target="_blank">Serious Games</a> Institute at Coventry University! Learn how the cell of Mandela will be build by us :)</p>
							<p class="info">posted on facebook.com/mandela27</p>
						</li>
						<li>
							<img class="icon" src="/images/twitter.png" />
							<p class="text">Today is 50 years to the day that #Mandela was arrested and imprisoned. The Trials of Riviona then continued to 1964. 
posted by <a href="https://twitter.com/Mandela27EU" target="_blank">@Mandela27EU</a> 11 July</p>
							<p class="info">posted by @Mandela27EU 28 Mar</p>
						</li>
						<li>
							<img class="icon" src="/images/twitter.png" />
							<p class="text">#mandela27 making the South African news as we went through passport control at Cape Town #euculture #mandela <a href="http://pic.twitter.com/e3toYfPs0i" target="_blank">pic.twitter.com/e3toYfPs0i</a></p>
							<p class="info">posted by @Mandela27EU 15 Mar</p>
						</li>
						<div class="clear"></div>
					</ul>
				</div>		
				<div id="link_blocks">
					<ul>
						<a href="/serious_game.php"><li class="odd button1"><p>Serious game<p></li></a>
						<a href="/event_launch.php"><li class=" button2"><p>Events</p></li></a>
						<a href="/learning_resources.php"><li class="odd button3"><p>Learning resources</p></li></a>
						<a href="/diy_exhibition.php"><li class="button4"><p>DIY Exhibition</p></li></a>
						<!--<a href="/cultural_map.php"><li class="odd button5"><p>Cultural Map</p></li></a>
						<a href="/e-tour.php"><li class="button6"><p>E-tour of Robben Island </p></li></a>-->
						<div class="clear"></div>
					</ul>
				</div>	
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
