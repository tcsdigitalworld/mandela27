<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Events</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">3 jun</span><span class="year">2014</span></div>
						<h2>Opening of Madela27 Exhibition at GAIA Museum in Denmark</h2>
						<p class="smalltitleorange">Randers, Denmark</p>					
					</div>					
					<div class="clear"></div><br />				
					<p class="short_intro">The first Danish Mandela27 Exhibiton opened 3rd June at GAIA Musuem of Outsider Art in Randers, Denmark.</p>
					<p><img src="files/events-gaia-extra.jpg" /></p>
					<p>Our Exhibition was shown together with an exhibition of prisoners' artwork. GAIA Museum specializes in the training of people with disabilities, such as autism or Down's syndrome. A group of enthusiasts built the exhibition which makes our concept a true teambuilding masterpiece!</p>
					<div class="backtoteam bottom">
						<a href="events.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>				
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
