<?php include('_header.php'); ?>
	<div class="maincontainer">		
		<div class="pagetitle">
			<div class="cen">
				<img src="images/years-left.png" />
				<div id="text">Projects Events</div>
				<img src="images/years-right.png" />
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="title">
						<h2>MANDELA27 EVENTS IN EUROPE AND SOUTH AFRICA</h2>
					</div>
					<p class="short_intro">Mandela27 is a cultural collaboration between the European Union and South Africa. Check our project events on both sides of the globe here.</p>
					<a href="project_events_upcoming.php">
						<div class="boxevents" style="background-image: url('files/eventshome-5.jpg');">
							<div class="eventstitle">
								<h2>Current &amp; Upcoming Events</h2>
							</div>
						</div>
					</a>
					<a href="project_events_past.php">
						<div class="boxevents" style="background-image: url('images/events_past.jpg');">
							<div class="eventstitle">
								<h2>Past Events</h2>
							</div>
						</div>
					</a>
				</div>
			</div>
			<div id="right">
				<?php include('_keepintouch.php'); ?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php include('_footer.php'); ?>