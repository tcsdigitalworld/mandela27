<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Events</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">23 apr</span><span class="year">2014</span></div>
						<h2>Spokespersons of the Year Awards 2013</h2>
						<p class="smalltitleorange">Vaal Triangle, South Africa</p>
					</div>					
					<div class="clear"></div><br />				
					<p class="short_intro">The North-West University recently held its annual Spokespersons of the Year-event. During this event, academics, researchers and other NWU faculty members are acknowledged for their positive contribution to the media landscape in an effort to uplift and enhance the institution.</p>
					<p><img src="files/events-press-event-nwu-extra1.jpg" /></p>
					<p>One of the Mandela27 partners -The South African Serious Gaming Institute- was invited to speak at the event about the graphic novel and serious game that was developed for Mandela27.</p>
					<p><img src="files/events-press-event-nwu-extra2.jpg" /></p>
					<div class="backtoteam bottom">
						<a href="events.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>				
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
