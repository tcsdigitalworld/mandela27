<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">TERMS & CONDITIONS</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="title">
						<h2>Copyright license</h2>
					</div>
					<p class="short_intro">The copyright in this website and the material on this website (including without limitation the text, computer code, artwork, photographs, images, music, audio material, video material and audio-visual material on this website) is owned by the Mandela27 consortium.</p>
				</div>
				<img src="files/island-1.jpg" alt="image" />
				<div class="clear"></div>
				<div class="content">
					<p>
The Mandela27 consortium grants to you a worldwide non-exclusive royalty-free revocable license to:
<ul>
<li>View this website and the material on this website on a computer or mobile device via a web browser;</li>
<li>Copy and store this website and the material on this website in your web browser cache memory; and</li>
<li>Print pages from this website for your own [personal and non-commercial] use.</li>
</ul>
The Mandela27 consortium does not grant you any other rights in relation to this website or the material on this website.   In other words, all other rights are reserved.</p>
<p>
For the avoidance of doubt, you must not adapt, edit, change, transform, publish, republish, distribute, redistribute, broadcast, rebroadcast or show or play in public this website or the material on this website (in any form or media) without The Mandela27 consortium's prior written permission.
</p>
					
				</div>
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
				?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
