<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Events</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">27 Nov</span></div>
						<h2>Mandela27 Kick-off meeting</h2>
						<p class="location">Coventry University, UK</p>
						<div class="clear"></div>
					</div>
				</div>
				<!--<img src="files/island-1.jpg" alt="image" />-->
				<div class="clear"></div>
				<div class="content">
					<p>The project team gathered for the first time in November 2012 at Coventry University. This was the first opportunity for the whole team to meet together and to start exchanging ideas that were emerging from the project's theme. Throughout the two days, there was a high level of excitement at the prospect of being engaged in such an important and relevant cultural project, including the potential to reach so many people with stories from Europe and South Africa. All partners came with a determination to plan and deliver a high quality project, and the meeting was carried out with good humor, bringing an immediate sense of camaraderie to the group. During the meeting the many upcoming project activities were discussed, and agreements were made about how to best proceed in order to meet both the project requirements and the expectations of all the partners.</p>
					<p>In addition to the core project work, visits were also made to key local cultural venues, the Herbert Gallery in Coventry, and the Shakespeare Birthplace Trust in Stratford upon Avon, both of whom have collections that resonate with impact of culture in both Europe and South Africa, and with the story of Nelson Mandela and Robben Island. We parted ways a clear vision of the work ahead of us, and the inspiration to carry it out.</p>
				</div>
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
				?>
				<div class="box">
					<h3>More events</h3>
					<ul class="eventlist">
						<a href="event_launch.php"><li>Mandela27 Project Meeting & Press Launch</li></a>
					</ul>	
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
