<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Events</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">11 mar</span></div>
						<h2>Project Meeting & Press Launch</h2>
						<p class="location">Robben Island & Cape Town, South Africa</p>
						<div class="clear"></div>
					</div>
				</div>
				<!--<img src="files/island-1.jpg" alt="image" />-->
				<div class="clear"></div>
				<div class="content">
					<p>For this second project meeting, the Mandela27 Team gathered in South Africa. As this is place of great inspiration for each and every one of the team, we were all very excited when we arrived in the city of Cape Town and honored, knowing that we would be spending most of our time on Robben Island, the place where Nelson Mandela was incarcerated and where history was written.</p>
					
					<p>Although the focus of this second project meeting was of course on discussing concrete agreements on how to deliver high quality concepts over the next few months, The Robben Island Museum-team also scheduled visits to Jetty 1, the historical gateway to Robben Island, the Robben Island Museum Mayibuye Archive at the University of the Western Cape and of course the Robben Island Maximum Security Prison itself. Especially this last stop, and the visit to Mr. Mandela's cell in particular, made a big impression on the team. The stories of the former political prisoners gave us clear insights of the life behind the walls of this prison and although the conditions were terrible, it was astonishing to see how these brave men found hope in their comradeship. We were all moved by these stories, told with so much passion by men so convinced that they have undergone these dreadful days, months and years on Robben Island with only one goal: fighting apartheid and pursuing freedom.</p>
					
					<p>These visits, along with the meetings, gave us great ideas on how to move forward. We left Robben Island with a suitcase full of plans, stories and pictures and headed back to Cape Town for the South African press launch of the Mandela27 Project. We were delighted to so how the South African press warmed up to our beautiful project and we were overwhelmed by the press coverage we got in the following days. </p>
					
					<p>This inspiring trip and all this attention gave us even more energy to deliver a fantastic project! </p>
					
					<p>Check out these international press articles to read all about the South African project presentation:</p>
					
					<ul>
						<li><a href="http://www.guardian.co.uk/world/2013/mar/14/robben-island-video-apartheid-history">Robben Island video game aims to teach young about Apartheid history</a> (The Guardian, Thursday 14 March 2013)</li>
						<li><a href="http://www.theregister.co.uk/2013/03/15/nelson_mandela_game/">Nelson Mandela's island prison hell to become game</a> (The Register, Friday 15th March 2013)</li>
						<li><a href="http://www.theaustralian.com.au/news/breaking-news/mandela-island-jail-becomes-computer-game/story-fn3dxix6-1226597757220">Mandela Island becomes computer game</a> (The Australian, Friday 15th March 2013)</li>
						<li><a href="http://www.timeslive.co.za/local/2013/03/14/new-interactive-mandela-project-launched">New Interactive Mandela project launched</a> (Times Live, Thursday 14 March 2013)</li>
						<li><a href="http://www.sowetanlive.co.za/news/mandela/2013/03/15/mandela-s-island-jail-a-computer-game">Mandela's Island Jail a Computer Game</a> (Sowetan Live, Friday 15th March 2013)</li>
						<li><a href="http://www.southafrica.info/mandela/mandela27-020413.htm#.UWLG6asd5Q9 d">Robben Island revisited digitally</a> (SouthAfrica.info, Monday 2nd April 2013)</li>
					</ul>	
				</div>
				
				<img src="files/news_pic_001.jpg" alt="image" />
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
				?>
				<div class="box">
					<h3>More events</h3>
					<ul class="eventlist">
						<a href="event_kickoff.php"><li>Mandela27 Kick-off meeting</li></a>
					</ul>	
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
