<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Current &amp; Upcoming Events</div>
					<img src="images/years-right.png" />
				</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="title">
						<h2>CURRENT &amp; UPCOMING MANDELA27 EVENTS</h2>
					</div>
					<p class="short_intro">Check if there's a Mandela27 event soon in town near you!</p>
					<p><img src="images/events-map-upcoming.jpg" alt="Upcoming Events Map" /></p><br />
					<div class="clear"></div>
					<p><strong>DIY EXHIBITIONS:</strong><img src="images/redthumbtack.png" /></p>
					<p><strong>SOUTH AFRICA:</strong><br />
						- KwaZulu Natal Museum, Pietermaritzburg (24 October 2014 – 1 February 2015)<br />
						- Western Cape Regional Works Unit, Guguletu (11 November 2014)<br />
						- Western Cape Regional Works Unit, Mthatha (1 December 2014)<br />
						- Fezeka High School, Guguletu (1 – 31 March 2015)
					</p>
					<p><strong>SWEDEN:</strong><br />
						- Nacka Bibliotek, Nacka (20 October – 6 December 2014)<br />
						- Haninge Bibliotek, Handen (6 December 2014 – 4 January 2015)<br />
						- Falu Bibliotek, Falu (4 January – 1 March 2015)
					</p>
					<p><strong>ITALY:</strong><br />
						<a href="http://festivalafricano.altervista.org/festival/?doing_wp_cron=1409581568.6108570098876953125000" target="_blank">- Festival del Cinema Africano, Verona (7 – 16 November 2014)</a><br />
						- Projecttomondo Mall, Verano (1 December 2014 – 31 January 2015
					</p>
					<p><strong>TEAM MEETINGS:</strong><img src="images/yellowthumbtack.png" /></p>
					<p><strong>BELGIUM:</strong><br />
						- <a href="http://mandela27.eu/event-30-10-2014.php" target="_blank">Official Project Presentation (30<sup>th</sup> October 2014)</a>
					</p>
					<div class="backtoteam bottom">
						<a href="project_events.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>			
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
				?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
