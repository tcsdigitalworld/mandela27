<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Events</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">22 jul</span><span class="year">2014</span></div>
						<h2>Opening of Mandela27 Exhibition at Manenberg Library</h2>
						<p class="smalltitleorange">Cape Town, South Africa</p>					
					</div>					
					<div class="clear"></div><br />				
					<p class="short_intro">Another wonderful set-up in South Africa. This time at the Manenberg Library where the Mandela Cell is being built by young learners with the help of library staff members.</p>
					<p><img src="files/manenberg-exhibition.jpg" /></p>
					<p>This cell is made of cardboard, another proof that our exhibition is a genuine Do-It-Yourself concept.  What a truly creative team!  On this occasion, the virtual tour and the serious game will be played inside the library, not in the cell itself.  There will be signs directing the learners to the computers where they will be able to log on the Mandela 27 website in the library.</p>
					<p>The picture below shows the building team, consisting of young chess champions. The impressive interpretation of the cell was officially opened by the an ex-political prisoner from this community.</p>
					<p><img src="files/events-manenberg-extra-pic.jpg" /></p>
					<div class="backtoteam bottom">
						<a href="events.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>				
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
