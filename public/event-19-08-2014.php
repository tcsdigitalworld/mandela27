<?php

include('_header.php');

?>
	<div class="maincontainer">
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Events</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">19 aug</span><span class="year">2014</span></div>
						<h2>Showcase of Mandela27 Serious Game at TechnoX</h2>
						<p class="smalltitleorange">Vaal Triangle, South Africa</p>
					</div>
					<div class="clear"></div><br />
					<p class="short_intro">North-West University brought the excitement around Mandela27 to the TechnoX event, hosted by Sosal at their Vaal Triange campus</p>
					<p>TechnoX is aimed at bringing local school children (ages from 13 to 18) in contact with technology and innovation institutions from all over South Africa.  SGI-SA secured a week-long daily slot at the event primarily to showcase the Mandela27 serious game and web portal.</p>
					<p>The Serious Gaming Institure (SGI-SA) secured a week-long daily slot at the event primarily to showcase the Mandela27 Serious Game and web portal.  Every day, 3 to 4 groups of 50+ school children passed by to see our project. Touching to see the way they drummed around the screens when the Mandela27 project came on and how their comradeship is almost tangible in the video!</p>
					<video width="490" height="345" poster="video/events-technox-v.jpg" controls="controls" preload="none">
						<!-- MP4 for Safari, IE9, iPhone, iPad, Android, and Windows Phone 7 -->
						<source type="video/mp4" src="video/events-technox.mp4" />
						<!-- Flash fallback for non-HTML5 browsers without JavaScript -->
						<object width="490" height="345" type="application/x-shockwave-flash" data="flashmediaelement.swf">
							<param name="movie" value="flashmediaelement.swf" />
							<param name="flashvars" value="controls=true&file=events-technox.mp4" />
							<!-- Image as a last resort -->
							<img src="video/events-technox-v.jpg" width="490" height="345" title="No video playback capabilities" />
						</object>
					</video>
					<div class="backtoteam bottom">
						<a href="events.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
