<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Events</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">26 apr</span><span class="year">2014</span></div>
						<h2>20th Freedom Day Celebration</h2>
						<p class="smalltitleorange">Cape Town, South Africa</p>
					</div>					
					<div class="clear"></div><br />				
					<p class="short_intro">My Freedom My Heritage - A symbol of freedom and reconciliation</p>
					<p><img src="files/events-freedom-day-extra.jpg" /></p>
					<p>South Africans celebrate Freedom Day each year on the 26th April. This years’ edition was the 20th and had a special meaning to all South Africans due to the decease of Nelson Mandela.  Robben Island Museum invited over 40 learners from local schools over a two-day team building to share history of the most notorious prison and of South Africa.</p>
					<div class="backtoteam bottom">
						<a href="events.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>				
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
