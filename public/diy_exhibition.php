<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">DIY Exhibition</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="title">
						<h2>Nelson Mandela’s cell on Robben island</h2>
					</div>
					<p class="short_intro">A series of educational resources for teachers and pupils/students will follow the DIY Exhibition.</p>
				</div>
				<img src="files/diyex.jpg" alt="image" />
				<div class="clear"></div>
				<div class="content">
					
						<p><p><span >The exhibition will follow the dimensions of Nelson Mandela’s cell on Robben island and can be constructed by following downloadable building instructions and using cheap, standard wood and plywood.</span></p><p>The interior will be bare except for the bedding material, a bench and a plate and cup that accompanied the prisoners in their cell.</p><p>Nine panels designed by students at NWU Vaal campus will be mounted to the exterior. The A2-panels are downloadable for builders of the DIY exhibition with printing equipment or a set can be ordered from the partners.</p><p>The panels will follow a graphic novel style together with material from UWC-Robben Island Museum Mayibuye Archives, which holds the Robben Island records and many of the records from organisations engaged in the struggle against apartheid.</p><p>The multimedia productions can be placed in or around the exhibition and display the following information:</p><p>
					<ul>

						<li><span>Background: Exploration, colonialism and slavery!</span></li>
					    <li><span>What is Apartheid?</span></li>
					    <li><span>Defiance: Sharpeville & Soweto;</span></li>
						<li><span>Fight for democracy: The people shall govern!</span></li> 
						<li><span>Robben island: Island prison;</span></li>
						<li><span>Mandela and the political prisoners: Culture, education and censorship in the prison;</span></li>
						<li><span>Don't entertain Apartheid! Support the cultural boycott (IDAF and international solidarity);</span></li>
						<li><span>How culture can influence change in society;</span></li>
						<li><span>Culture, reconciliation and citizenship.</span></li>
					</ul>
					</p>
				</div>

			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
				?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
