<?php

include('_header.php');

?>
	<div class="maincontainer">
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Events</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">24 oct</span><span class="year">2014</span></div>
						<h2>KwaZulu-Natal Museum hosts Mandela27 Exhibition</h2>
						<p class="smalltitleorange">Pietermaritzburg, South Africa</p>
					</div>
					<div class="clear"></div><br />
					<p class="short_intro">KwaZulu-Natal Museum ranks among the top National Museums in South Africa's, a cultural and natural history museum renowned for its unique collections.</p>
					<p>There are eight natural history and around ten cultural history galleries that include an array of mammals (together with the last wild elephant in KwaZulu-Natal), birds, amphibians, insects, an extensive mollusc collection (there are few South African land snails that are not represented in the Natal Museum) and a life-size T-Rex model. There is also a room dedicated to KwaZulu-Natal history, geological and Paleontological material, and a reconstruction of a Victorian street set in the late 1800's, complete with shops, stable and period homes. Take a walk through a life size recreation of a Drakensberg cave with rock art drawings or a walk on the wooden deck of a wrecked trading vessel in our Towns and Trade Exhibition. What makes the KwaZulu-Natal Museum particularly interesting is the array of interesting temporary exhibitions that give the museum a definite dynamic advantage? Temporary exhibitions and happenings have included an exploration of the history of segregation in the US, xenophobic violence, children’s art exhibition. The Museum officially opened an exciting exhibition on the Soccer World Cup, in May 2010. The 150th anniversary of the arrival of Indian people to South Africa was celebrated in November 2010, with the launch of a new permanent exhibition showcasing the Indian community of Pietermaritzburg. The KwaZulu-Natal Museum is also very popular with the schools and family visitors alike. The museum boasts an internet café with an impressive 16 computers, resource library for learners and film room.</p>
					<p><img src="files/events-kwazulu.jpg" /></p>
					<div class="backtoteam bottom">
						<a href="events.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
