<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">PUBLICATIONS</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<a href="http://hutchinscenter.fas.harvard.edu/transition-116">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">31 oct</span><span class="year">2014</span></div>
							<h2>Preview – Transition Issue 116: Remembering Nelson Mandela</h2>
							<p class="smalltitle">Harvard, UK</p>
						</div>
						<p class="white">In December, we mark a year since the passing of Nelson Mandela—a man who was as much myth as flesh and blood. With issue 116, we pay tribute to Mandela’s worldly attainments and to his otherworldly sainthood.<br />
						<span class="orange">Transition by Hutchins Center for African & African American Research</span></p>
					</div>
				</a>
				<div class="clear"></div>
				<a href="http://www.iol.co.za/capetimes/exhibition-honours-struggle-veterans-1.1751972#.VE5VNqtDv2c">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">17 sep</span><span class="year">2014</span></div>
							<h2>Exhibition honours Struggle veterans</h2>
							<p class="smalltitle">Cape Town, South Africa</p>
						</div>
						<p class="white">For the younger generation, seldom does the opportunity arise to view the Struggle against apartheid through the eyes of those who lived it from within its smallest confines – a prison cell.<br />
						<span class="orange">CapeTimes</span></p>
					</div>
				</a>
				<div class="clear"></div>
				<a href="http://allafrica.com/stories/201409171307.html">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">17 sep</span><span class="year">2014</span></div>
							<h2>South Africa: Surviving Rivonia Trialists Hold Dialogue With School Learners</h2>
							<p class="smalltitle">Cape Town, South Africa</p>
						</div>
						<p class="white">Learners from a number of schools in Cape Town had the opportunity to meet surviving Rivonia trialists when a replica of Nelson Mandela's prison cell on Robben Island was launched to tour the country.<br />
						<span class="orange">AllAfrica.com</span></p>
					</div>
				</a>
				<div class="clear"></div>
				<a href="http://www.sabc.co.za/news/a/06769e80457eec98ac33afc7c599c9eb/Nelson-Mandelas-prison-cell-launched-at-V-&-A-Waterfront-20141609">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">16 sep</span><span class="year">2014</span></div>
							<h2>Nelson Mandela’s prison cell launched at V&A Waterfront</h2>
							<p class="smalltitle">Cape Town, South Africa</p>
						</div>
						<p class="white">The Robben Island Museum has launched a replica model of Nelson Mandela's prison cell at the V & A Waterfront<br />
						<span class="orange">SABC News</span></p>
					</div>
				</a>
				<div class="clear"></div>
				<a href="http://www.vallentuna.se/sv/Uppleva-och-gora/Kultur/Kulturhuset/Konst-och-utstallningar/Konst-och-utstallningar/Alla/Miniutstallning-Mandela27/">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">10 sep</span><span class="year">2014</span></div>
							<h2>Mandela27 Exhibtion at Vallentuna Kulturhus och Bibliotek</h2>
							<p class="smalltitle">Vallentuna, Sweden</p>
						</div>
						<p class="white">Our Mandela27 Exhibition is becoming more impressive at each new launch. Don't hesitate to visit the latest set-up at the Vallentuna Library in Sweden!<br />
						<span class="orange">Vallentuna Kommun website</span></p>
					</div>
				</a>
				<div class="clear"></div>
				<a href="images/delft-madiba-day.mp4">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">19 jul</span><span class="year">2014</span></div>
							<h2>Event report on Mandela27 Exhibition at Delft Civic Centre</h2>
							<p class="smalltitle">Delft, South Africa</p>
						</div>
						<p class="white">One of our South African partners, Robben Island Museum, has celebrated Mandela Day with the set-up of several Mandel27 Exhibitions. One of them was at Delft Civic Centre in Delft.<br />
						<span class="orange">Robben Island Museum</span></p>
					</div>
				</a>
				<div class="clear"></div>
				<a href="http://www.polskieradio.pl/13/2772/Artykul/1171440">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">5 jul</span><span class="year">2014</span></div>
							<h2>Interview with Polish radio by Alex Woolner</h2>
							<p class="smalltitle">Coventry, UK</p>
						</div>
						<p class="white">Interview for the EU Euranet ..(17.30)<br />
						<span class="orange">Polish Radio II</span></p>
					</div>
				</a>
				<div class="clear"></div>
				<a href="http://www.bbc.co.uk/news/uk-england-coventry-warwickshire-26985269">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">11 apr</span><span class="year">2014</span></div>
							<h2>Nelson Mandela prison cell re-created in Coventry</h2>
							<p class="smalltitle">Coventry, UK</p>
						</div>
						<p class="white">Nelson Mandela's Robben Island jail cell has been recreated for an exhibition marking the 27 years he spent in prison. Coventry University worked with students in South Africa to create the interactive exhibition and digital 'game' exploring Mandela's struggle against apartheid.<br />
						<span class="orange">England’s Big Picture, BBC News</span></p>
					</div>
				</a>
				<div class="clear"></div>
				<a href="http://www.thebusinessdesk.com/westmidlands/news/603775-lifestyle-serious-games-institute-supports-new-nelson-mandela-exhibition.html?news_section=54022">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">11 apr</span><span class="year">2014</span></div>
							<h2>Serious Games Institute supports new Nelson Mandela exhibition</h2>
							<p class="smalltitle">Coventry, UK</p>
						</div>
						<p class="white">A NEW exhibition inspired by Nelson Mandela’s efforts to end apartheid opens at the Herbert Art Gallery &amp; Museum in Coventry tomorrow (Saturday).Led by Coventry University’s Serious Games Institute.<br />
						<span class="orange">TheBusinessDesk.com</span></p>
					</div>
				</a>
				<div class="clear"></div>
				<a href="http://clg.coventry.gov.uk/info/42/guidance_notes_and_general_information/1145/mandela27_project">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">11 apr</span><span class="year">2014</span></div>
							<h2>Mandela27 Project</h2>
							<p class="smalltitle">Coventry, UK</p>
						</div>
						<p class="white">I am writing to let you know about the learning resources available through the Mandela27 project. This Culture project is led by the Serious Games Institute at Coventry University with partners in South Africa and Europe.<br />
						<span class="orange">Coventry City Council</span></p>
					</div>
				</a>
				<div class="clear"></div>
				<a href="video/interview-alex-woolner.mp4" class="mp4vid">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">11 apr</span><span class="year">2014</span></div>
							<h2>Interview on Mandela27 Project</h2>
							<p class="smalltitle">Coventry, UK</p>
						</div>
						<p class="white">Interview with Alex Woolner of Coventry University about the Mandela27 Project. <br />
						<span class="orange">Coventry University</span></p>
					</div>
				</a>
				<div class="clear"></div>
				<a href="http://www.theherbert.org/whats-on/events-exhibitions/mandela27">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">11 apr</span><span class="year">2014</span></div>
							<h2>The Mandela27 installation is a cultural exchange between the European Union and South Africa.</h2>
							<p class="smalltitle">Coventry, UK</p>
						</div>
						<p class="white">It examines the cultural changes that occurred in Europe and South Africa through the lens of Nelson Mandela’s struggle against apartheid and his time spent in Robben Island Prison.<br />
						<span class="orange">TheHerbert.org</span></p>
					</div>
				</a>
				<div class="clear"></div>
				<a href="http://www.seriousgamesinstitute.co.uk/events/pages.aspx?section=29&item=228">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">11 apr</span><span class="year">2014</span></div>
							<h2>Mandela 27 at the Herbert Gallery</h2>
							<p class="smalltitle">Coventry, UK</p>
						</div>
						<p class="white">From Friday 11th April at The Herbert Gallery and Museum you can see an artist’s replica in the actual size of Nelson Mandela’s cell, his home for 27 years while imprisoned at Robben Island.<br />
						<span class="orange">SeriousGameInstiture.co.uk</span></p>
					</div>
				</a>
				<div class="clear"></div>
				<a href="http://blogs.coventry.ac.uk/researchblog/mandela-27-about-the-project/">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">21 mar</span><span class="year">2014</span></div>
							<h2>Mandela27: About the project</h2>
							<p class="smalltitle">Coventry, UK</p>
						</div>
						<p class="white">Imagine a physical display of Nelson Mandela’s cell the same dimension as his cell in Robben Island; alongside it several photographic displays and three screens showing the cultural platform which takes the form of the plan of Robben Island.<br />
						<span class="orange">Coventry University Research Blog</span></p>
					</div>
				</a>
				<div class="clear"></div>
				<a href="http://www.brussels.be/artdet.cfm?id=4843&agendaid=4333">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">15 mar</span><span class="year">2014</span></div>
							<h2>Tribute to Nelson Mandela</h2>
							<p class="smalltitle">Brussels, Belgium</p>
						</div>
						<p class="white">On the occasion of the International Day Against Racism and Discrimination, the City of Brussels holds a tribute to Nelson Mandela with Exhibition 'Mandela27' with a representation of the cell of Nelson Mandela on Robben Island.<br />
						<span class="orange">www.brussels.be</span></p>
					</div>
				</a>
				<div class="clear"></div>
				<a href="www.gozuidafrika.com/nieuws/nieuws_detail.php?newsID=5863">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">15 mar</span><span class="year">2014</span></div>
							<h2>Mandela27, meer dan alleen een verhaal over Robbeneiland</h2>
							<p class="smalltitle">Brussels, Belgium</p>
						</div>
						<p class="white">Een nieuwe internationale campagne, waarbij België mee in investeert, moet Nelson Mandela's 27 jaar gevangenschap op Robbeneiland, en de invloed die het had op Zuid-Afrika en de wereld, digitaal in beeld gaan brengen.<br />
						<span class="orange">GoZuidAfrika.com</span></p>
					</div>
				</a>
				<div class="clear"></div>
				<a href="http://www.culturefund.eu/projects/mandela-27-years-a-cultural-experience-across-the-european-union-and-south-africa">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">5 mar</span><span class="year">2014</span></div>
							<h2>A cultural experience</h2>
							<p class="smalltitle">London, UK</p>
						</div>
						<p class="white">Mandela27 is a collaboration between EU and South Africa (SA) based on the iconic story of Nelson Mandela and the globally recognised Robben Island Museum.<br />
						<span class="orange">Interm Creative Europe Desk – Culture</span></p>
					</div>
				</a>		
				<div class="clear"></div>
				<a href="http://www.a-n-s.be/activiteiten/culturele-activiteiten/zavel-marollen-tentoonstelling-nelson-mandela">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">23 feb</span><span class="year">2014</span></div>
							<h2>De Zavel en de Marollen - Tentoonstelling Nelson Mandela</h2>
							<p class="smalltitle">Brussels, Belgium</p>
						</div>
						<p class="white">Vandaag trekken we naar Brussel. Onze gids neemt ons mee naar 2 extreme wijken. Nergens in Brussel is er een groter contrast te vinden dan tussen de Zavel en de Marollen. We nemen beide buurten onder de loep. De grenzen tussen hoog en laag, rijk en arm, chique en authentiek worden deze keer letterlijk overschreden. Na de wandeling trekken we naar het Belvue museum waar we de gratis tijdelijke tentoonstelling van Nelson Mandela bezoeken.<br />
						<span class="orange">ANS.be</span></p>
					</div>
				</a>
				<div class="clear"></div>
				<a href="http://vaalnews.nwu.ac.za/n/en/281">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">21 feb</span><span class="year">2014</span></div>
							<h2>1st International Conference on Serious Games</h2>
							<p class="smalltitle">Vaal Triangle, South Africa</p>
						</div>
						<p class="white">It is all systems go for the first <a href="http://www.sgisa.co.za/conf2014/">International Conference on Serious Games</a>, which is set to take place in August this year.<br />
						<span class="orange">North-West University Website</span></p>
					</div>
				</a>
				<div class="clear"></div>
				<a href="http://carteprof.be/news_view.php?ne_id=351">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">21 jan</span><span class="year">2014</span></div>
							<h2>News: Expo Mandela au Musée BELvue</h2>
							<p class="smalltitle">Brussels, Belgium</p>
						</div>
						<p class="white">Le musée BELvue, à Bruxelles, accueille jusqu'au 13 mars Mandela27, une exposition internationale de passage à Bruxelles, qui plonge le visiteur dans la vie de Nelson Mandela durant ses 27 années de détention.<br />
						<span class="orange">Carteprof.be</span></p>
					</div>
				</a>
				<div class="clear"></div>
				<a href="http://belvue.be/en/activities/exposition-mandela27">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">9 jan</span><span class="year">2014</span></div>
							<h2>Mandela27: the fight against apartheid from behind bars</h2>
							<p class="smalltitle">Brussels, Belgium</p>
						</div>
						<p class="white">This free exhibition is a perfect complement to your other activities or visit to the BELvue museum. It's a small exhibition (the size of Mandela's cell on Robben Island), so it is better to combine it with another activity.<br />
						<span class="orange">BELvue Museum </span></p>
					</div>
				</a>		
				<div class="clear"></div>
				<a href="http://www.southafrica.be/wp-content/uploads/2014/02/mandela27-EN.pdf">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">9 jan</span><span class="year">2014</span></div>
							<h2>Mandela27 Free Exhibition</h2>
							<p class="smalltitle">Brussels, Belgium</p>
						</div>
						<p class="white">You can visit the international Mandela27 Exhibition in the BELvue Museum from 9 January 2014 through 13 March 2014.<br />
						<span class="orange">Embassy of the Republic of South Africa</span></p>
					</div>
				</a>		
				<div class="clear"></div>
				<a href="http://www.kbs-frb.be/pressitem.aspx?id=309489&Member=13442&langtype=2067">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">9 jan</span><span class="year">2014</span></div>
							<h2>Democratie in de kijker in BELvue museum </h2>
							<p class="smalltitle">Brussels, Belgium</p>
						</div>
						<p class="white">Een maand na het overlijden van Nelson Mandela, opent in het BELvue museum in Brussel een kleine, maar opvallende, tentoonstelling, "Mandela 27" over het moderne Zuid-Afrika met als leidraad de figuur van de voormalige president en iconische leider.<br />
						<span class="orange">Koning Boudewijn Stichting</span></p>
					</div>
				</a>		
				<div class="clear"></div>
				<a href="http://www.wegwijzerwoi.be/nl/mandela27-_-tentoonstelling-over-nelson-mandela-belvue-museum">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">9 jan</span><span class="year">2014</span></div>
							<h2>Mandela27 : Tentoonstelling over Nelson Mandela</h2>
							<p class="smalltitle">Brussels, Belgium</p>
						</div>
						<p class="white">In de tentoonstelling 'Mandela27' in het BELvue Museum in Brussel word je ondergedompeld in het leven van Nelson Mandela gedurende de 27 jaren van zijn gevangenschap.<br />
						<span class="orange">Gemeenschapsonderwijs.be</span></p>
					</div>
				</a>		
				<div class="clear"></div>
				<a href="http://www.gemeenschapsonderwijs.be/Net_Prikbord/Artikel.aspx?Id=7862&type=Deeltijds%20kunstonderwijs">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">9 jan</span><span class="year">2014</span></div>
							<h2>Mandela27 : In de voetsporen van Nelson Mandela</h2>
							<p class="smalltitle">Brussels, Belgium</p>
						</div>
						<p class="white">Nelson Mandela werd niet alleen zowat de bekendste vrijheidsstrijder van de 20e eeuw, maar ook bijna 100 jaar. In 2013 overleed hij, maar zijn verzoenende en verbindende gedachtengoed blijft hopelijk verder leven. Museum BELvue geeft het goede voorbeeld.<br />
						<span class="orange">WegWijzer WOI</span></p>
					</div>
				</a>		
				<div class="clear"></div>
				<a href="http://www.klascement.net/kalender/46048/">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">9 jan</span><span class="year">2014</span></div>
							<h2>Mandela27' : Tentoonstelling over Nelson Mandela</h2>
							<p class="smalltitle">Brussels, Belgium</p>
						</div>
						<p class="white">In de tentoonstelling 'Mandela27' in het BELvue Museum in Brussel word je ondergedompeld in het leven van Nelson Mandela gedurende de 27 jaren van zijn gevangenschap. <br />
						<span class="orange">KlasCement Leermiddelennetwerk</span></p>
					</div>
				</a>		
				<div class="clear"></div>
				<a href="http://www.kulturradet.se/EU_Kulturprogram/Nyheter/Mandela-27/" target="_blank">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">17 oct</span><span class="year">2013</span></div>
							<h2>Mandela27</h2>
							<p class="smalltitle">Stockholm, Sweden</p>
						</div>
						<p class="white">The Swedish company Elderberry is one of five project partners in the project Mandela27. (article in Swedish)<br />
						<span class="orange">Kulturradet.se</span></p>
					</div>
				</a>
				<div class="clear"></div>
				<a href="publication-08-07-2013.php">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">8 july</span><span class="year">2013</span></div>
							<h2>What Mandela27 does for youngsters</h2>
							<p class="smalltitle">Brussels, Belgium</p>
						</div>
						<p class="white">Mandela27 examines how Culture has brought about change in Europe and South Africa through the lens of Nelson Mandela's struggle against apartheid.<br />
						<span class="orange">Kim, Mandela27</span></p>
					</div>
				</a>		
				<div class="clear"></div>
				<a href="http://sverigesradio.se/sida/artikel.aspx?programid=1646&artikel=5527171">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">14 may</span><span class="year">2013</span></div>
							<h2>Mandela prison becomes computer game</h2>
							<p class="smalltitle">Stockholm, Sweden</p>
						</div>
						<p class="white">The South African freedom fighter Nelson Mandela was imprisoned for 27 years. Most of the time, he was interned on Robben Island. Today, this notorious prison becomes an interactive experience through a collaboration between South Africa and the EU. (interview in Swedish)<br />											
						<span class="orange">P3 Radiostation</span></p>
					</div>
				</a>
				<div class="clear"></div>
				<a href="http://www.southafrica.info/mandela/mandela27-020413.htm#.UWLG6asd5Q9d#ixzz2lCaIHIzF">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">2 apr</span><span class="year">2013</span></div>
							<h2>Robben Island revisited digitally</h2>
							<p class="smalltitle">Cape Town, South Africa</p>
						</div>
						<p class="white">A multimedia project, Mandela 27, will bring the tales of Robben Island to life digitally as part of a new international campaign.<br />
						<span class="orange">SouthAfrica.info</span></p>	
					</div>
				</a>
				<div class="clear"></div>
				<a href="http://www.taipeitimes.com/News/feat/archives/2013/03/18/2003557345">				
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">18 mar</span><span class="year">2013</span></div>
							<h2>Virtual history</h2>
							<p class="smalltitle">Cape Town, South Africa</p>
						</div>
						<p class="white">A Robben Island video game aims to teach young Sout Africans about apartheid<br />
						<span class="orange">Taipei Times</a></p>
					</div>
				</a>
				<div class="clear"></div>
				<a href="http://www.bbc.co.uk/worldserviceradio">				
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">18 mar</span><span class="year">2013</span></div>
							<h2>Interview Jacqueline Cawston on Mandela27</h2>
							<p class="smalltitle">Coventry, United Kingdom<br />
							<span class="orange">BBC Radio</span></p>
						</div>					
					</div>
				</a>
				<div class="clear"></div>
				<a id="march-15"></a>
				<a href="http://www.theregister.co.uk/2013/03/15/nelson_mandela_game/">					
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">15 mar</span><span class="year">2013</span></div>
							<h2>Nelson Mandela's island prison hell to become game</h2>
							<p class="smalltitle">Cape Town, South Africa</p>
						</div>
					<p class="white">Computer game violence continues to raise hackles around the world, having been fingered as a cause of gun violence in the USA and suggested as an underminer of civility just about everywhere else.<br />
					<span class="orange">The Register</span></p>
					</div>
				</a>
				<div class="clear"></div>
				<a href="http://www.theaustralian.com.au/news/latest-news/mandela-island-jail-becomescomputer-game/story-fn3dxix6-1226597757220">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">15 mar</span><span class="year">2013</span></div>
							<h2>Mandela island jail becomes computer game</h2>
							<p class="smalltitle">Cape Town, South Africa</p>
						</div>
						<p class="white">Gaming fans will soon be able to navigate Nelson Mandela's South African apartheid prison on Robben Island in a new computer game, developers have announced.<br />
						<span class="orange">The Australian</span></p>
					</div>
				</a>
				<div class="clear"></div>
				<a href="http://www.gate5.co.za/qv/pr/19197370/20864795/0/p">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">15 mar</span><span class="year">2013</span></div>
							<h2>New game depicts life on Robben Island</h2>
							<p class="smalltitle">Cape Town, South Africa</p>
						</div>
						<p class="white">New game depicts life on Robben Island<br />
						<span class="orange">Sowetan</span></p>
					</div>
				</a>
				<div class="clear"></div>
				<a href="http://bnn-news.com/video-game-teach-mandelas-27-years-prison-90839">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">15 mar</span><span class="year">2013</span></div>
							<h2>Video game to teach about Mandela's 27 years in prison</h2>
							<p class="smalltitle">Cape Town, South Africa</p>
						</div>
						<p class="white">A video game launched within Mandela 27 cultural project, named after former South African president Nelson Mandela's years in jail, will aim to teach the young about apartheid.<br />
						<span class="orange">Baltic News Network</span></p>
					</div>
				</a>
				<div class="clear"></div>
				<a href="http://www.sowetanlive.co.za/news/mandela/2013/03/15/mandela-s-island-jail-acomputer-game">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">15 mar</span><span class="year">2013</span></div>
							<h2>Mandela's Island Jail a Computer Game</h2>
							<p class="smalltitle">Cape Town, South Africa</p>
						</div>
						<p class="white">Gaming fans will soon be able to navigate Nelson Mandela's South African apartheid prison on Robben Island in a new computer game, developers announced.<br />
						<span class="orange">Sowetanlive.co.za</span></p>
					</div>
				</a>
				<div class="clear"></div>
				<a href="">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">15 mar</span><span class="year">2013</span></div>
							<h2>New Mandela project launched</h2>
							<p class="smalltitle">Johannesburg, South Africa</p>
						</div>
						<p class="white">A new interactive project detailing former president Nelson Mandela's imprisonment on Robben Island and its effects on the world was unveiled on Thursday.<br />
						<span class="orange">News24.com</span></p>
						
					</div>
				</a>
				<div class="clear"></div>
				<a href="http://www.gettyimages.be/detail/nieuwsfoto's/jacqueline-cawston-of-the-seriousgames-institute-nieuwsfotos/163670687?Language=nl">			
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">14 mar</span><span class="year">2013</span></div>
							<h2>Press Conference Mandela27</h2>
							<p class="smalltitle">Cape Town, South Africa</p>
						</div>
						<p class="white">Jacqueline Cawston (R) of the Serious Games Institute Coventry University (Britain), talks on March 14, 2013 during a press conference at the V&A Waterfront in Cape Town.<br />
						<span class="orange">Gettyimages.com</span></p>
						
					</div>
				</a>	
				<div class="clear"></div>
				<a href="http://www.theguardian.com/world/2013/mar/14/robben-island-video-apartheid-history">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">14 mar</span><span class="year">2013</span></div>
							<h2>Robben Island Video game aims to teach young about apartheid history</h2>
							<p class="smalltitle">Cape Town, South Africa</p>
						</div>
						<p class="white">Mandela 27 cultural project, named after former South African president's years in jail, ...<br />
						<span class="orange">The Guardian</span></p>
					</div>
				</a>
				<div class="clear"></div>
				<a href="http://www.timeslive.co.za/local/2013/03/14/new-interactive-mandela-project-launched">
					<div class="content small">
						<div class="newstitle">
							<div class="datecontainer"><span class="date">14 mar</span><span class="year">2013</span></div>
							<h2>New interactive mandela project launched</h2>
							<p class="smalltitle">Cape Town, South Africa</p>
						</div>
						<p class="white">A new interactive project detailing former president Nelson Mandela's imprisonment on Robben Island and its effects on the world was unveiled on Thursday.<br />
						<span class="orange">Timeslive.co.za</span></p>
					</div>
				</a>
				<div class="clear"></div>				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
