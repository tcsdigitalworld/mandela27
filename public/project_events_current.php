<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">CURRENT Events</div>
					<img src="images/years-right.png" />
				</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="title">
						<h2>CURRENT MANDELA27 EVENTS</h2>
					</div>
					<p class="short_intro">Check if the Mandela27 Exhibition is up somewhere near you!</p>
					<p><img src="images/events-map-current.jpg" alt="Current Events Map" /></p><br />
					<div class="clear"></div>
					<p><strong>DIY EXHIBITIONS:</strong><img src="images/redthumbtack.png" /></p>
					<p><strong>SOUTH AFRICA:</strong><br />
						- False Bay FET Colleges, South Africa (Tour from July to September 2014)<br />
						&nbsp; &nbsp; 1. Khayelitsha Campus<br />
						&nbsp; &nbsp; 2. Muizenberg Campus<br />
						&nbsp; &nbsp; 3. Fish Hoek Campus<br />
						&nbsp; &nbsp; 4. Weslake Campus<br />
						&nbsp; &nbsp; 5. Mitchell’s Plain Campus<br /><br />
						- Manenberg City Library, Cape Town (18th July to 30th September 2014)<br /><br />
						- North-West University, Vaal Triangle Campus (1st August – 31 October 2014)
					</p>
					<div class="backtoteam bottom">
						<a href="project_events.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
				?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
