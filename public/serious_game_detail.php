<?php

include('_header.php');

?>
	<div class="maincontainer">
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">SERIOUS GAME</div>
					<img src="images/years-right.png" />
				</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="title">
						<h2>EXPERIENCE THE LIFE AS A POLITICAL PRISONER AT ROBBEN ISLAND</h2>
					</div>
					<p class="orange">The project team created an interactive graphic novel showing what life for political prisoners was like at Robben Island Prison by making use of Games Based Learning principle.</p>
					<p>The serious game, in the form of a graphic novel dotted with interactive scenes, is primarily aimed at Generation Y from both Europe and South Africa. That is not to say that other generations will not find the graphic novel compelling. South African students are creating the content/artwork based on actual events related to us by ex political prisoners in a way that will appeal to their peers while remaining sensitive to the reality that was Robben Island Prison.</p>
					<p>Through discussion with the ex prisoners we have identified 5 recurring elements that will form the core content of the graphic novel.  These include:<br />
						- Prisoners were not allowed to see children under the age of 16 as they were not allowed on the island.<br />
						- The importance that prisoners attached to physical activity in order to keep their bodies healthy.<br />
						- The extent in which communication to their loved ones was censored and how this drove the prisoners to smuggle documents and other notes to the outside world.<br />
						- The fact that they made use of hunger strikes to rally for better prison conditions and basic human rights in the island prison.<br />
						- The struggle for obtaining the right to education and how the older prisoners made use of their sessions of hard labour at the quarries to educate (even teaching them to read and write) the younger ones.</p>
					<p><a class="linkbg smallerlink game" style="display:block" href="game/index.html">Play the game online</a></p>
					<p><a onclick="return conf();" class="linkbg smallerlink" style="display:block" href="game/M27_Win_Final.zip">Download game for PC</a></p>
					<p><a onclick="return conf();" class="linkbg smallerlink" style="display:block" href="game/M27_Mac_Final.zip">Download game for MAC</a></p>
					<p><a onclick="return conf();" class="linkbg smallerlink" style="display:block" href="game/Mandela27.apk">Download game on your mobile</a></p>
				</div>
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
				?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
