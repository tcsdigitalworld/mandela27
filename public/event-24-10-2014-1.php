<?php

include('_header.php');

?>
	<div class="maincontainer">
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Events</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">24 oct</span><span class="year">2014</span></div>
						<h2>Silvio D’Arzo first Italian school to inaugurate Mandela27 Exhibition</h2>
						<p class="smalltitleorange">Montecchio Emilia, Italy</p>
					</div>
					<div class="clear"></div><br />
					<p class="short_intro">Istituto d'Istruzione Superiore Silvio D'Arzo in Montecchio Emilia is the first to launch the Mandela27 Exhibition in Italy.</p>
					<p>The Exhibition will be displayed until the 20th of December!	</p>
					<p><img src="files/events-arzo.jpg" /></p>
					<div class="backtoteam bottom">
						<a href="events.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
