<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">teacher notes</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="title">
						<h2>Additional Learning Resources</h2>
					</div>
					<p class="orange">The Mandela27 Learning Resources provide all needed information to raise the topics that are displayed on the posters of the Mandela27 DIY Exhibition and interact with your public,</p>
					<p>These Mandela27 Learning Resources are available:</p>
					<p><strong>WEBQUEST:</strong><br />
						In support of the Cultural Timeline, the Serious Game as well as the DIY Exhibition, we set various webquest problem based tasks to be solved using the Mandela27 material in combination with on-line research.</p>
					<p><strong>NOTES FOR TEACHERS:</strong><br />
						The teacher notes follow the poster headings of the DIY Exhibition and contain texts, questions and answers for teachers to use with pupils and students. The teacher notes address the background to apartheid and the fight for freedom and democracy in South Africa however the main focus is on what it means for young people to be a citizen today.</p>
					<p>The teacher notes raise the following topics:</p>
					<p>
						- Introduction to Mandela27<br />
						- Colonialism and slavery!<br />
						- What is Apartheid?<br />
						- Defiance: Sharpeville & Soweto;<br />
						- Fight for democracy: The people shall govern!<br />
						- Robben island: Island prison;<br />
						- Mandela and the political prisoners: Culture, education and censorship in the prison;<br />
						- Don't entertain Apartheid! Support the cultural boycott (IDAF and international solidarity);<br />
						- How culture can influence change in society;<br />
						- Culture, reconciliation and citizenship.</p>
					<p>The teacher notes are now available in English, Dutch, French and Afrikaans and will soon be available in Italian and Swedish.</p>
					<p><strong>FORUM:</strong><br />
						An on-site forum for teachers to discuss educational aspects of the material and to get feedback from experts will be set up shortly.</p>
					<p><strong>LEARNING NETWORK:</strong><br />
						Teachers will be invited to join a network group to be linked to the existing learning networks run by Elderberry AB at www.smile-vet.eu and www.sharinglandscapes.eu. A sub-group will be formed in each forum on the Mandela27 project.</p>
					<div class="biglink" style="bottom:-55px;">
						<p class="techerheading"><a style="line-height:30px" href="download.php?what=teach">Download the free Mandela 27 teacher notes now</a></p>
						<!--
						<p class="techerheading">Download the free Mandela27 DIY Exhibition POSTERS.</p>						
							<a href="download.php?what=learn">Swedish</a>
							<a href="download.php?what=learn">French</a>
							<a href="download.php?what=learn">Dutch</a>					
							<a href="download.php?what=learn">English</a>
							<a href="download.php?what=learn">Afrikaans</a>
							<a href="download.php?what=learn">Xhosa</a>
						//-->
					</div>
				</div>				
				<div class="clear"></div>
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
				?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
