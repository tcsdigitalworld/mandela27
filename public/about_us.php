<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">The team</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="title">
						<h2>About the team</h2>
					</div>
					<p><img src="images/about-team.jpg"></p>
					<p class="orange">The Mandela27-project is a true collaboration. The partners involved have been working together and evolving the idea over 2 years.</p>
					<p>The lead partner, Coventry University's Serious Games Institute is a centre of excellence in cultural games and applications. Elderberry has vast experience of learning through culture. The Vaal Triangle Campus of North West University in South Africa is trying to grow an industry in cultural games and have a strong connection with the UK. Robben Island Museum is a world recognised UNESCO heritage site with a wealth of cultural artefacts and cultural learning. They will lend their expertise in sensitive cultural subjects to the EU. And TCS Digital World is situated in the heart of the EU in Brussels with a dynamic human-centred approach to cultural dissemination.</p>
					<p>Over the course the 2 year project, each partner country will host team meetings during which key plans and milestones will be created and reviewed. These meetings are also an opportunity for the team to briefly immerse themselves in cultural iconography of the host location, whether this be Robben Island prison, Shakespeare?s writing (an inspiration to Mandela), Swedish politics (instrumental in the fight against apartheid) or European Capital Brussels (home of the Cultural program).</p>

					<ul class="about_team_list">
						<li><a class="logo-coventry" href="partners/sgi.php"></a></li>
						<li><a class="logo-tcsdw" href="partners/tcsdw.php"></a></li>
						<li><a class="logo-elderberry" href="partners/elderberry.php"></a></li>
						<li><a class="logo-northwestun" href="partners/nwu.php"></a></li>
						<li><a class="logo-robbenisland" href="partners/rim.php"></a></li>						
						<div class="clear"></div>
					</ul>
				</div>
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
				?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
