<?php include('_header.php'); ?>
<link href="js/video-js/video-js.css" rel="stylesheet" type="text/css">
<script src="js/video-js/video.js"></script>
<script>
	videojs.options.flash.swf = "js/video-js/video-js.swf";
</script>

	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Events</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">18 jul</span><span class="year">2014</span></div>
						<h2>Opening of Madela27 Exhibition at Delft Civic Centre</h2>
						<p class="smalltitleorange">Delft, South Africa</p>					
					</div>					
					<div class="clear"></div><br />				
					<p class="short_intro">One of our South African partners, Robben Island Museum, has celebrated Mandela Day with the set-up of several Mandel27 Exhibitions. One of the many venues is the Delft Civic Centre in Delft.</p>
					<p><img src="files/events-delft-extra1.jpg" /></p>
					<p>The launch was done by the organisation called Help To Care SA. Members of the Correctional Services were in charge of assembling the cell at Drakenstein Correctional Services and did it a second Delft Civic Centre. Young Delft cadets were impressed by the exhibition and paid honor to Madiba and South Africa.</p>
					<p><img src="files/events-delft-extra2.jpg" /></p>
					<p>Members of the Western Cape Regional Works Unit and members of the Correctional Services posing in front of Mandela Cell.</p>
					<p>Check the event report here.</p>
					<video id="video_1" class="video-js vjs-default-skin" controls preload="none" width="484" height="272" data-setup="{}">
						<source src="images/delft-madiba-day.mp4" type='video/mp4' />
						<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
					</video>
					<div class="backtoteam bottom">
						<a href="events.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>				
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
