<?php

include('_header.php');

?>
	<div class="maincontainer">
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Events</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<div class="newstitle">
						<div class="datecontainer"><span class="date">16 sep</span><span class="year">2014</span></div>
						<h2>Opening of Mandela27 Exhibition at Robben Island Museum Gateway</h2>
						<p class="smalltitleorange">Cape Town, South Africa</p>
					</div>
					<div class="clear"></div><br />
					<p class="short_intro">Today, the Mandela27 Exhibition opened at the Gateway to Robben Island. This launch is a big milestone for Mandela27 as our Exhibition is finally put up at Robben Island Museum, the UNESCO World Heritage Site and our project partner.</p>					
					<p>The official launch was combined with an exclusive conversation with the 3 surviving Rivonia Trialists, Dr. Ahmed Kathrada, Dr. Andrew Mlangeni and Professor Denis Goldberg. Check the <a href="files/reports-by-the-src.pdf" style="text-decoration:underline;">event report</a> by one of the students that attended!</p>
					<p><img src="files/events-gateway.png"></p>
					<div class="backtoteam bottom">
						<a href="events.php"><img src="images/arrow_left_small.png" alt="image" />Back to overview</a>
					</div>
				</div>
				<div class="clear"></div>
				
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
