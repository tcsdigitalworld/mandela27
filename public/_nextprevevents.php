<div class="box">
	<h3>Following events</h3>
	<ul class="eventlist">
		<a href="event-02-01-2014.php"><li>02/01/2014 - Mandela27 official presentation - Brussels, Belgium </li></a>		
	</ul>	
</div>
<div class="box">
	<h3>Previous events</h3>
	<ul class="eventlist">
		<a href="event-11-09-2013.php"><li>11/09/2013 - Project Meeting - Stockholm, Sweden</li></a>
		<a href="event-27-02-2013.php"><li>27/02/2013 - Project meeting & press launch - Robben Island & Cape Town, South Africa</li></a>		
		<a href="event-27-11-2012.php"><li>27/11/2012 - Project Kick-off - Coventry, United Kingdom</li></a>
	</ul>	
</div>