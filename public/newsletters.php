<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">NEWSLETTER</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<a target="_blank" class="brown" href="http://us2.campaign-archive2.com/?u=eeb1c6ea98aec42a2ff0220b7&id=a83c24a294">October 23st, 2014</a><br />
					<a target="_blank" class="whitelink" href="http://us2.campaign-archive2.com/?u=eeb1c6ea98aec42a2ff0220b7&id=a83c24a294">Mandela27 Final Event – Press Invitation</a>
				</div>
				<div class="content">
					<a target="_blank" class="brown" href="http://us2.campaign-archive2.com/?u=eeb1c6ea98aec42a2ff0220b7&id=5d64b80978">October 22st, 2014</a><br />
					<a target="_blank" class="whitelink" href="http://us2.campaign-archive2.com/?u=eeb1c6ea98aec42a2ff0220b7&id=5d64b80978">Mandela27 Final Event – Practical Details</a>
				</div>
				<div class="content">
					<a target="_blank" class="brown" href="http://us2.campaign-archive1.com/?u=eeb1c6ea98aec42a2ff0220b7&id=933ad49eea">October 21st, 2014</a><br />
					<a target="_blank" class="whitelink" href="http://us2.campaign-archive1.com/?u=eeb1c6ea98aec42a2ff0220b7&id=933ad49eea">Mandela27 Exhibition Results</a>
				</div>
				<div class="content">
					<a target="_blank" class="brown" href="http://us2.campaign-archive1.com/?u=eeb1c6ea98aec42a2ff0220b7&id=3b403bb0b5">September 19th, 2014</a><br />
					<a target="_blank" class="whitelink" href="http://us2.campaign-archive1.com/?u=eeb1c6ea98aec42a2ff0220b7&id=3b403bb0b5">Mandela27 Update September</a>
				</div>
				<div class="content">
					<a target="_blank" class="brown" href="http://us2.campaign-archive1.com/?u=eeb1c6ea98aec42a2ff0220b7&id=76dd864e86&e=%5bUNIQID">September 4th, 2014</a><br />
					<a target="_blank" class="whitelink" href="http://us2.campaign-archive1.com/?u=eeb1c6ea98aec42a2ff0220b7&id=76dd864e86&e=%5bUNIQID">Invitation Mandela27 Kick-Off Event</a>
				</div>
				<div class="content">
					<a target="_blank" class="brown" href="http://us2.campaign-archive2.com/?u=eeb1c6ea98aec42a2ff0220b7&id=59772ff790">March 20th, 2014</a><br />
					<a target="_blank" class="whitelink" href="http://us2.campaign-archive2.com/?u=eeb1c6ea98aec42a2ff0220b7&id=59772ff790">Mandela27 - 21st of March activities</a>
				</div>
			</div>			
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
