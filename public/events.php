<?php

include('_header.php');

?>
	<div class="maincontainer">		
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Events</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">				
				<div class="content">
					<a href="event-30-10-2014.php">
						<div class="boxevents" style="background-image: url('files/eventshome-1.jpg');">
							<div class="eventstitle">
								<h2>Mandela27 Project Kick-Off</h2>
								<p class="short_intro">Brussels, Belgium</p>
							</div>
							<div class="datebox"><span class="date">30 oct</span><span class="year">2014</span></div>
						</div>
					</a>
					<a href="event-24-10-2014-1.php">
						<div class="boxevents" style="background-image: url('files/events-arzo.jpg');">
							<div class="eventstitle">
								<h2>Silvio D’Arzo first Italian school to inaugurate Mandela27 Exhibition</h2>
								<p class="short_intro">Montecchio Emilia, Italy</p>
							</div>
							<div class="datebox"><span class="date">24 oct</span><span class="year">2014</span></div>
						</div>
					</a>
					<a href="event-24-10-2014.php">
						<div class="boxevents" style="background-image: url('files/events-kwazulu.jpg');">
							<div class="eventstitle">
								<h2>KwaZulu-Natal Museum hosts Mandela27 Exhibition</h2>
								<p class="short_intro">Pietermaritzburg, South Africa</p>
							</div>
							<div class="datebox"><span class="date">24 oct</span><span class="year">2014</span></div>
						</div>
					</a>
					<a href="event-20-10-2014.php">
						<div class="boxevents" style="background-image: url('files/events-nacka.jpg');">
							<div class="eventstitle">
								<h2>Nacka Forum Public Library hosts Mandela 27</h2>
								<p class="short_intro">Nacka, Sweden</p>
							</div>
							<div class="datebox"><span class="date">20 oct</span><span class="year">2014</span></div>
						</div>
					</a>
					<a href="event-10-10-2014.php">
						<div class="boxevents" style="background-image: url('files/events-afrovibes.jpg');">
							<div class="eventstitle">
								<h2>Afrovibes and The Drum are proud to host Mandela 27</h2>
								<p class="short_intro">Birmingham, UK</p>
							</div>
							<div class="datebox"><span class="date">10 oct</span><span class="year">2014</span></div>
						</div>
					</a>
					<a href="event-22-09-2014.php">
						<div class="boxevents" style="background-image: url('files/events-central-library.jpg');">
							<div class="eventstitle">
								<h2>Opening of Mandela27 Exhibition at Cape Town Central Library</h2>
								<p class="short_intro">Cape Town, South Africa</p>
							</div>
							<div class="datebox"><span class="date">22 sep</span><span class="year">2014</span></div>
						</div>
					</a>
					<a href="event-15-09-2014.php">
						<div class="boxevents" style="background-image: url('files/events-gateway.png');">
							<div class="eventstitle">
								<h2>Opening of Mandela27 Exhibition at Robben Island Museum Gateway</h2>
								<p class="short_intro">Cape Town, South Africa</p>
							</div>
							<div class="datebox"><span class="date">16 sep</span><span class="year">2014</span></div>
						</div>
					</a>
					<a href="event-03-09-2014.php">
						<div class="boxevents" style="background-image: url('files/events-vallentuna.jpeg');">
							<div class="eventstitle">
								<h2>Opening of Mandela27 Exhibition at Vallentuna Library</h2>
								<p class="short_intro">Vallentuna, Sweden</p>
							</div>
							<div class="datebox"><span class="date">3 sep</span><span class="year">2014</span></div>
						</div>
					</a>
					<a href="event-28-08-2014.php">
						<div class="boxevents" style="background-image: url('files/events-icsg.jpg');">
							<div class="eventstitle">
								<h2>Coventry University's Professor of the Serious Games visits South Africa</h2>
								<p class="short_intro">Vaal Triangle, South Africa</p>
							</div>
							<div class="datebox"><span class="date">28 aug</span><span class="year">2014</span></div>
						</div>
					</a>
					<a href="event-19-08-2014.php">
						<div class="boxevents" style="background-image: url('files/events-technox.jpg');">
							<div class="eventstitle">
								<h2>Showcase of Mandela27 Serious Game at TechnoX</h2>
								<p class="short_intro">Vaal Triangle, South Africa</p>
							</div>
							<div class="datebox"><span class="date">19 aug</span><span class="year">2014</span></div>
						</div>
					</a>
					<a href="event-12-08-2014.php">
						<div class="boxevents" style="background-image: url('images/manenberg.jpg');">
							<div class="eventstitle">
								<h2>Opening of Mandela27 Exhibition at Manenberg Community Library</h2>
								<p class="short_intro">Manenberg, South Africa</p>
							</div>
							<div class="datebox"><span class="date">12 aug</span><span class="year">2014</span></div>
						</div>
					</a>
					<a href="event-22-07-2014.php">
						<div class="boxevents" style="background-image: url('files/events-manenberg.jpg'); background-repeat:no-repeat;">
							<div class="eventstitle">
								<h2>Opening of Mandela27 Exhibition at Manenberg Library</h2>
								<p class="short_intro">Cape Town, South Africa</p>
							</div>
							<div class="datebox"><span class="date">22 jul</span><span class="year">2014</span></div>
						</div>
					</a>
					<a href="event-18-07-2014-2.php">
						<div class="boxevents" style="background-image: url('files/events-delft-exhibition.jpg'); background-repeat:no-repeat;">
							<div class="eventstitle">
								<h2>Opening of Madela27 Exhibition at Delft Civic Centre</h2>
								<p class="short_intro">Delft, South Africa</p>
							</div>
							<div class="datebox"><span class="date">18 jul</span><span class="year">2014</span></div>
						</div>
					</a>
					<a href="event-18-07-2014-1.php">
						<div class="boxevents" style="background-image: url('files/events-national-library.jpg'); background-repeat:no-repeat;">
							<div class="eventstitle">
								<h2>Opening of Madela27 Exhibition at the National Library of South Africa</h2>
								<p class="short_intro">Pretoria, South Africa</p>
							</div>
							<div class="datebox"><span class="date">18 jul</span><span class="year">2014</span></div>
						</div>
					</a>
					<a href="event-18-07-2014.php">
						<div class="boxevents" style="background-image: url('files/events-drakenstein.jpg'); background-repeat:no-repeat;">
							<div class="eventstitle">
								<h2>Opening of Madela27 Exhibition at Drakenstein Correctional Services</h2>
								<p class="short_intro">Paarl, South Africa</p>
							</div>
							<div class="datebox"><span class="date">18 jul</span><span class="year">2014</span></div>
						</div>
					</a>
					<a href="event-03-06-2014.php">
						<div class="boxevents" style="background-image: url('files/events-gaia.jpg'); background-repeat:no-repeat;">
							<div class="eventstitle">
								<h2>Opening of Madela27 Exhibition at GAIA Museum in Denmark</h2>
								<p class="short_intro">Randers, Denmark</p>
							</div>
							<div class="datebox"><span class="date">3 jun</span><span class="year">2014</span></div>
						</div>
					</a>
					<a href="event-26-04-2014.php">
						<div class="boxevents" style="background-image: url('files/events-freedom-day-celebration.jpg'); background-repeat:no-repeat;">
							<div class="eventstitle">
								<h2>20th Freedom Day Celebration</h2>
								<p class="short_intro">Cape Town, South Africa</p>
							</div>
							<div class="datebox"><span class="date">26 apr</span><span class="year">2014</span></div>
						</div>
					</a>
					<a href="event-23-04-2014.php">
						<div class="boxevents" style="background-image: url('files/events-press-event-nwu.jpg'); background-repeat:no-repeat;">
							<div class="eventstitle">
								<h2>Spokespersons of the Year Awards 2013</h2>
								<p class="short_intro">Vaal Triangle, South Africa</p>
							</div>
							<div class="datebox"><span class="date">23 apr</span><span class="year">2014</span></div>
						</div>
					</a>
					<a href="event-11-04-2014.php">
						<div class="boxevents" style="background-image: url('files/events-herbert.jpg'); background-repeat:no-repeat;">
							<div class="eventstitle">
								<h2>Opening of Mandela27 Exhibtion at The Herbert Gallery and Museum in UK</h2>
								<p class="short_intro">Coventry, UK</p>
							</div>
							<div class="datebox"><span class="date">11 apr</span><span class="year">2014</span></div>
						</div>
					</a>
					<a href="event-21-03-2014-1.php">
						<div class="boxevents" style="background-image: url('files/events-human-rights-day.jpg'); background-repeat:no-repeat;">
							<div class="eventstitle">
								<h2>Human Rights Day Celebration</h2>
								<p class="short_intro">Brussels, Belgium</p>
							</div>
							<div class="datebox"><span class="date">21 mar</span><span class="year">2014</span></div>
						</div>
					</a>
					<a href="event-21-03-2014.php">
						<div class="boxevents" style="background-image: url('files/born-free-generation-big.jpg'); background-repeat:no-repeat;">
							<div class="eventstitle">
								<h2>21 March, International Day for the Elimination of Racial Discrimination</h2>
								<p class="short_intro">Europe &amp; South Africa</p>
							</div>
							<div class="datebox"><span class="date">21 mar</span><span class="year">2014</span></div>
						</div>
					</a>
					<a href="event-07-01-2014.php">
						<div class="boxevents" style="background-image: url('files/eventshome-5.jpg');">
							<div class="eventstitle">
								<h2>Opening of first DIY Exhibition</h2>
								<p class="short_intro">Brussels, Belgium</p>
							</div>
							<div class="datebox"><span class="date">7 jan</span><span class="year">2014</span></div>
						</div>
					</a>					
					<a href="event-06-12-2013.php">
						<div class="boxevents" style="background-image: url('files/mandela_info_1.jpg'); background-repeat:no-repeat;">
							<div class="eventstitle">
								<h2>Thank you, Madiba</h2>
								<p class="short_intro">Mandela27 Project Team</p>
							</div>
							<div class="datebox"><span class="date">6 dec</span><span class="year">2013</span></div>
						</div>
					</a>
					<a href="event-11-09-2013.php">
						<div class="boxevents" style="background-image: url('files/eventshome-2.jpg');">
							<div class="eventstitle">
								<h2>Project Meeting</h2>
								<p class="short_intro">Stockholm, Sweden</p>
							</div>
							<div class="datebox"><span class="date">11 sep</span><span class="year">2013</span></div>
						</div>
					</a>
					<a href="event-27-02-2013.php">
						<div class="boxevents" style="background-image: url('files/eventshome-3.jpg');">
							<div class="eventstitle">
								<h2>project meeting & press launch</h2>
								<p class="short_intro">Robben Island & Cape Town, South Africa</p>
							</div>
							<div class="datebox"><span class="date">27 feb</span><span class="year">2013</span></div>
						</div>
					</a>
					<a href="event-27-11-2012.php">
						<div class="boxevents" style="background-image: url('files/eventshome-4.jpg');">
							<div class="eventstitle">
								<h2>Project Kick-off</h2>
								<p class="short_intro">Coventry, United Kingdom</p>
							</div>
							<div class="datebox"><span class="date">27 nov</span><span class="year">2012</span></div>
						</div>
					</a>					
				</div>
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
					include('_nextprevevents.php')
				?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
