<?php

include('_header.php');

?>
	<div class="maincontainer">
		<div class="pagetitle">
				<div class="cen">
					<img src="images/years-left.png" />
					<div id="text">Download</div>
					<img src="images/years-right.png" />
				</div>
			</div>
			<div class="clear"></div>
	</div>
	<div class="clear"></div>

	<div class="maincontainer page">
		<div id="content" class="page">
			<div id="left">
				<div class="content">
					<?php
					if(isset($_POST['submit']) && $_POST['submit'] == "Download") {

						$sw = 0;
						if(empty($_POST['firstname'])) {
							$sw = 1; }
						if(empty($_POST['lastname'])) {
							$sw = 1; }
						if(empty($_POST['function'])) {
							$sw = 1; }
						if(empty($_POST['email'])) {
							$sw = 1; }
						if(empty($_POST['typeofvenue'])) {
							$sw = 1; }
						if(empty($_POST['namevenue'])) {
							$sw = 1; }
						if(empty($_POST['countryvenue'])) {
							$sw = 1; }

						if($sw==0) {

							if($_POST['type']=="buildins") {
								$type = 'Building Instructions';
							} elseif($_POST['type']=="posters") {
								$type = 'Posters';
							} elseif($_POST['type']=="teach") {
								$type = 'Teacher Notes';
							} else {
								$type = 'Learning Notes';
							}

							$msg = "Sender's information:<br>\n";
							$msg .= "------------------------------------------------------<br>\n";
							$msg .= "<strong>First Name:</strong>		".$_POST['firstname']."<br>\n";
							$msg .= "<strong>Last Name:</strong>		".$_POST['lastname']."<br>\n";
							$msg .= "<strong>Function:</strong>			".$_POST['function']."<br>\n";
							$msg .= "<strong>E-Mail:</strong>			".$_POST['email']."<br>\n";
							$msg .= "<strong>Type of venue:</strong>	".$_POST['typeofvenue']."<br>\n";
							$msg .= "<strong>Name of enue:</strong>		".$_POST['namevenue']."<br>\n";
							$msg .= "<strong>Country of venue:</strong>	".$_POST['countryvenue']."<br>\n";
							$msg .= "<strong>Request Type:</strong>		".$type;

							$m->From ($_POST['email'], $_POST['firstname']." ".$_POST['lastname']);
							$m->To('kimv@tcsdigitalworld.com');
							$m->Subject("Download From [".date('d/M/Y')."]");
							$m->Body($msg);
							$m->Priority(3);
							$m->Send();

							echo '<div class="title">
									<h2>Thank you</h2>';
									if($_POST['type']=="buildins") {
										echo '<p class="short_intro">Please click on the link below to start downloading the Mandela27 Exhibition building instructions.<br /><br /><a href="files/Building-Instructions.pdf">Download</a></p>';
									} elseif($_POST['type']=="posters") {
										echo'
										<p class="short_intro">
											Please click on the link below to start downloading the Mandela27 Exhibition posters.<br /><br />
											<a href="files/Postersets-fr.pdf">French</a><br />
											<a href="files/italian-posters.zip">Italian</a><br />
											<a href="files/Posterset-nl.pdf">Dutch</a><br />
											<a href="files/posters-english.zip">English</a><br />
											<a href="files/Mandela27-poster-Afrikaans.zip">Afrikaans</a><br /><br />
											Or contact the project partner in your country to apply for printed posters:<br />
											Belgium: <a href="mailto:kimv@tcsdigitalworld.com">Kim Vilein</a> at TCS Digital World<br />
											UK: <a href="mailto:AWoolner@cad.coventry.ac.uk">Alex Woolner</a> at Coventry University<br />
											Sweden: <a href="mailto:elderberryab@gmail.com">David Powell</a> at Elderberry Culture Projects<br />
											South Africa: <a href="mailto:NomatshayinaM@robben-island.org.za">Nomatshayina Mfeketho</a> at Robben Island Museum
										</p>';
									} elseif($_POST['type']=="learn") {
										echo'
										<p class="short_intro">
											Please click on the link below to start downloading the Mandela27 Exhibition learning notes.<br /><br />
											<a href="files/Postersets-fr.pdf">French</a><br />
											<a href="files/Posterset-nl.pdf">Dutch</a><br />
											<a href="files/Posterset-en.pdf">English</a>
										</p>';
									} elseif($_POST['type']=="teach") {
										echo'
										<p class="short_intro">
											Thank you<br />Please click on the link below to start downloading the Mandela 27 Teacher Notes.<br /><br />
											<a href="/files/Mandela27-Pistes-Pedagogique-FR.pdf">French</a><br />
											<a href="/files/Mandela27-Pedagogisch-Dossier-NL.pdf">Dutch</a><br />
											<a href="/files/Mandela27-Teachter-Notes-ENG.pdf">English</a><br />
											<a href="/files/Mandela27-Teacher-Notes-Afrikaans.pdf">Afrikaans</a>
										</p>';
									}elseif($_POST['type']=="game") {
                    echo '
                    <p class="short_intro">
											Thank you<br />Please click on the link below to start downloading the Mandela 27 standalone Serious Game.<br /><br />
											<a href="/files/M27_Mac_Final.zip">Mandela 27 Serious Game for Mac</a><br />
											<a href="/files/M27_Win_Final.zip">Mandela 27 Serious Game for Windows</a><br />
											<a href="/files/M27_Web_Final.zip">Mandela 27 Serious Game for Web</a>
										</p>
                    ';
                  }
							echo '</div>';

						} else {

							echo '<p style="color:#FF0000;">Please fill in all fields marked with *</p>';

						}

					} else { ?>
						<div class="title">
							<?php if($_GET['what']=="buildins") { ?>
								<p>Thank you for downloading the Mandela27 Exhibition building instructions. Please fill in this application form to be redirected to the download page:</p>
							<?php } elseif($_GET['what']=="posters") { ?>
								<p>Thank you for downloading the Mandela27 Exhibition posters. Please fill in this application form to be redirected to the download page:</p>
							<?php } elseif($_GET['what']=="learn") { ?>
								<p>Thank you for downloading the Mandela27 Exhibition learning notes. Please fill in this application form to be redirected to the download page:</p>
							<?php } elseif($_GET['what']=="teach") { ?>
								<p>Thank you for downloading the Mandela27 Teacher Notes. Please fill in this application form to be redirected to the download page:</p>
							<?php } elseif($_GET['what']=="game") { ?>
                <p>Thank you for downloading the Mandela27 standalone Serious Game. Please fill in this application form to be redirected to the download page:</p>
              <?php } ?>
							<p>&nbsp;</p>
						</div>
						<form method="post" action="download.php">
							<input type="hidden" name="type" value="<?php echo $_GET['what']; ?>" />
							<label for="firstname">First Name*:</label><input class="inputfield" type="text" name="firstname" id="firstname" />
							<div class="clear"></div>
							<label for="lastname">Last name*:</label><input class="inputfield" type="text" name="lastname" id="lastname" />
							<div class="clear"></div>
							<label for="function">Function*:</label><input class="inputfield" type="text" name="function" id="function" />
							<div class="clear"></div>
							<label for="email">E-mail*:</label><input class="inputfield" type="text" name="email" id="email" />
							<div class="clear"></div>
							<label name="typeofvenue">Type of venue*:</label>
							<select class="inputfielddrop" name="typeofvenue" id="typeofvenue">
								<option>School</option>
								<option>Gallery</option>
								<option>Museum</option>
								<option>Cultural centre</option>
								<option>Other</option>
							</select>
							<div class="clear"></div>
							<label for="namevenue">Name of venue*:</label><input class="inputfield" type="text" name="namevenue" id="namevenue" />
							<div class="clear"></div>
							<label for="countryvenue">Country of venue*:</label><input class="inputfield" type="text" name="countryvenue" id="countryvenue" />
							<div class="clear"></div>
							<input type="submit" name="submit" value="Download" class="download" />
							<div class="clear"></div>
						</form>
			        <?php } ?>
				</div>
			</div>
			<div id="right">
				<?php
					include('_keepintouch.php');
				?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php

include('_footer.php');

?>
